package edu.gatech.adamlab.genii.shared;

public class Clock
{
	private long iniTime = 0;
	private float checktime = 0.0f;
	private float deltatime = 0.0f;

	public Clock()
	{
		iniTime = System.currentTimeMillis();
	}

	public void start()
	{
		iniTime = System.currentTimeMillis();
	}

	public void check()
	{
		float time = (System.currentTimeMillis() - iniTime) / 1000.0f;
		deltatime = time - checktime;
		checktime = time;
		// deltatime = 0.02f;
		// checktime += deltatime;
	}

	public void checkMillis()
	{
		float time = System.currentTimeMillis() - iniTime;
		deltatime = time - checktime;
		checktime = time;
		// deltatime = 0.02f;
		// checktime += deltatime;
	}

	public float time()
	{
		return checktime;
	}

	public float deltatime()
	{
		return deltatime;
	}

	public float iniTime()
	{
		return (System.currentTimeMillis() - iniTime) / 1000.0f;
	}

	public float iniTimeMillis()
	{
		return (System.currentTimeMillis() - iniTime);
	}
}