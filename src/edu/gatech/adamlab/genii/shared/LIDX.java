package edu.gatech.adamlab.genii.shared;

import java.util.HashMap;

import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;

public enum LIDX
{
	LowerTorso, UpperTorso, Neck, Head,
	CollarLeft, UpperArmLeft, ForeArmLeft, HandLeft, FingersLeft, ThumbLeft,
	CollarRight, UpperArmRight, ForeArmRight, HandRight, FingersRight, ThumbRight,
	PelvisLeft, ThighLeft, ShinLeft, FootLeft,
	PelvisRight, ThighRight, ShinRight, FootRight;

	public static HashMap<LIDX, Pair<JIDX, JIDX>> _LIMB_ENDS = null;

	public static HashMap<LIDX, Pair<JIDX, JIDX>> LIMB_ENDS()
	{
		if (null != _LIMB_ENDS)
			return _LIMB_ENDS;

		_LIMB_ENDS = new HashMap<LIDX, Pair<JIDX, JIDX>>();
		_LIMB_ENDS.put(LIDX.LowerTorso, Pair.of(JIDX.SpineBase, JIDX.SpineMid));
		_LIMB_ENDS.put(LIDX.UpperTorso, Pair.of(JIDX.SpineMid, JIDX.SpineShoulder));
		_LIMB_ENDS.put(LIDX.Neck, Pair.of(JIDX.SpineShoulder, JIDX.Neck));
		_LIMB_ENDS.put(LIDX.Head, Pair.of(JIDX.Neck, JIDX.Head));
		_LIMB_ENDS.put(LIDX.CollarLeft, Pair.of(JIDX.SpineShoulder, JIDX.ShoulderLeft));
		_LIMB_ENDS.put(LIDX.UpperArmLeft, Pair.of(JIDX.ShoulderLeft, JIDX.ElbowLeft));
		_LIMB_ENDS.put(LIDX.ForeArmLeft, Pair.of(JIDX.ElbowLeft, JIDX.WristLeft));
		_LIMB_ENDS.put(LIDX.HandLeft, Pair.of(JIDX.WristLeft, JIDX.HandLeft));
		_LIMB_ENDS.put(LIDX.FingersLeft, Pair.of(JIDX.HandLeft, JIDX.HandTipLeft));
		_LIMB_ENDS.put(LIDX.ThumbLeft, Pair.of(JIDX.HandLeft, JIDX.ThumbLeft));
		_LIMB_ENDS.put(LIDX.CollarRight, Pair.of(JIDX.SpineShoulder, JIDX.ShoulderRight));
		_LIMB_ENDS.put(LIDX.UpperArmRight, Pair.of(JIDX.ShoulderRight, JIDX.ElbowRight));
		_LIMB_ENDS.put(LIDX.ForeArmRight, Pair.of(JIDX.ElbowRight, JIDX.WristRight));
		_LIMB_ENDS.put(LIDX.HandRight, Pair.of(JIDX.WristRight, JIDX.HandRight));
		_LIMB_ENDS.put(LIDX.FingersRight, Pair.of(JIDX.HandRight, JIDX.HandTipRight));
		_LIMB_ENDS.put(LIDX.ThumbRight, Pair.of(JIDX.HandRight, JIDX.ThumbRight));
		_LIMB_ENDS.put(LIDX.PelvisLeft, Pair.of(JIDX.SpineBase, JIDX.HipLeft));
		_LIMB_ENDS.put(LIDX.ThighLeft, Pair.of(JIDX.HipLeft, JIDX.KneeLeft));
		_LIMB_ENDS.put(LIDX.ShinLeft, Pair.of(JIDX.KneeLeft, JIDX.AnkleLeft));
		_LIMB_ENDS.put(LIDX.FootLeft, Pair.of(JIDX.AnkleLeft, JIDX.FootLeft));
		_LIMB_ENDS.put(LIDX.PelvisRight, Pair.of(JIDX.SpineBase, JIDX.HipRight));
		_LIMB_ENDS.put(LIDX.ThighRight, Pair.of(JIDX.HipRight, JIDX.KneeRight));
		_LIMB_ENDS.put(LIDX.ShinRight, Pair.of(JIDX.KneeRight, JIDX.AnkleRight));
		_LIMB_ENDS.put(LIDX.FootRight, Pair.of(JIDX.AnkleRight, JIDX.FootRight));
		return _LIMB_ENDS;
	}
}
