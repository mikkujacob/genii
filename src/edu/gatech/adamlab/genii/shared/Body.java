package edu.gatech.adamlab.genii.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;

import java.io.Serializable;

public class Body implements Serializable, Cloneable
{

	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;
	
	private JIDX anchorJoint;
	
	private HashMap<JIDX, PVector> jointPositions;
	
	private HashMap<JIDX, Quaternion> jointAngles;
	
	public Body()
	{
		this.jointPositions = new HashMap<JIDX, PVector>();
		for(JIDX index : JIDX.values())
		{
			jointPositions.put(index, new PVector());
		}
		this.jointAngles = new HashMap<JIDX, Quaternion>();
		for(JIDX index : JIDX.values())
		{
			jointAngles.put(index, new Quaternion());
		}
		this.anchorJoint = null;
	}
	
	public Body(HashMap<JIDX, PVector> jointPositions, HashMap<JIDX, Quaternion> jointAngles, JIDX anchorJoint)
	{
		this.jointPositions = jointPositions;
		this.jointAngles = jointAngles;
		this.anchorJoint = anchorJoint;
	}

	public PVector[] localBasis()
	{
		PVector localFwddir = PVecUtilities.orthonormalization(orientation(),
				PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis =
		{ localSidedir, PVecUtilities.UPVEC, localFwddir };
		return localBasis;
	}

	public PVector center()
	{
		return getJointPosition(JIDX.SpineBase);
	}

	public Body transformed(PVector[] sourceBasis, PVector[] targetBasis,
			PVector targetCenter)
	{
		Body transformedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.values())
		{
			if (JIDX.SpineBase == jidx)
			{
				transformedPose.setJointPosition(JIDX.SpineBase, targetCenter);
			}
			else
			{
				PVector jointPosition = getJointPosition(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector,
						sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(
						targetBasis, dissolution);
				transformedPose.setJointPosition(jidx,
						PVector.add(targetCenter, normalizedPosition));
			}
		}
		return transformedPose;
	}

	public Body translated(PVector targetCenter)
	{
		Body translatedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.values())
		{
			if (JIDX.SpineBase == jidx)
			{
				translatedPose.setJointPosition(jidx, targetCenter);
			}
			else
			{
				PVector jointPosition = getJointPosition(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				translatedPose.setJointPosition(jidx,
						PVector.add(targetCenter, radiusVector));
			}
		}
		return translatedPose;
	}

	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(getJointPosition(JIDX.SpineBase),
				getJointPosition(JIDX.ShoulderLeft), getJointPosition(JIDX.ShoulderRight));
	}

	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(getJointPosition(JIDX.Neck), getJointPosition(JIDX.SpineBase));
		upvec.div(upvec.mag());
		return upvec;
	}

	public PVector lowerUpvec()
	{
		return PVecUtilities.UPVEC;
	}

	@Override
	public Object clone()
	{
		Body cln = new Body();

		for (JIDX jidx : this.getJointPositions().keySet())
		{
			cln.setJointPosition(jidx, PVecUtilities.clone(getJointPosition(jidx)));
		}

		return cln;
	}

	// interpolates a position of the body from the positions and their
	// coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient)
	{

		// position of the body to be returned
		Body body = new Body();
		
		// to store the part of the body which position is being computed
		JIDX jidx;
		
		// browses all parts of the body
		for(int i = 0; i < JIDX.values().length; i++)
		{
			// part of the body to interpolate
			jidx = JIDX.values()[i];
			
			// interpolates the position of this part of the body and stores it
			body.setJointPosition(jidx, PVector.lerp(body1.getJointPosition(jidx),
					body2.getJointPosition(jidx), coefficient));
			
			// interpolates the angle of this part of the body and stores it
			body.setJointAngle(jidx, Quaternion.slerp(body1.getJointAngle(jidx),
					body2.getJointAngle(jidx), coefficient));
		}
		
		// anchor joint for new body
		body.setAnchorJoint(body1.anchorJoint);

		// the interpolated position of the body is returned
		return body;

	}
	
	// Calculates the centroid of the body from all bodies in an ArrayList<Body>
	@SuppressWarnings("deprecation")
	public static Body centroid(ArrayList<Body> bodies)
	{
		// position of the body to be returned
		Body body = (Body)bodies.get(0).clone();

		// to store the part of the body which position is being computed
		JIDX jpidx;

		for(int i = 1; i < bodies.size(); i++)
		{
			Body body1 = bodies.get(i);
			// browses all parts of the body
			for (Map.Entry<JIDX, PVector> joints_iterator : body1.getJointPositions().entrySet())
			{
				//Incremental average
				//mt = mt-1 + (at - mt-1) / t
				// part of the body to centroid
				jpidx = joints_iterator.getKey();
				PVector prototypeJoint = body.getJointPosition(jpidx);
				PVector currentJoint = body1.getJointPosition(jpidx);
				
				body.setJointPosition(jpidx, PVector.add(prototypeJoint, PVector.div(PVector.sub(currentJoint, prototypeJoint), i)).get());
			}
		}

		// the centroid position of the body is returned
		return body;

	}

	public boolean isSimilar(Body other, float relativeDeviationTolerance)
	{
		PVector centerA = getJointPosition(JIDX.SpineBase);
		PVector centerB = other.getJointPosition(JIDX.SpineBase);
		for (JIDX jidx : JIDX.values())
		{
			if (JIDX.SpineBase == jidx)
				continue;
			PVector radialA = PVector.sub(getJointPosition(jidx), centerA);
			PVector radialB = PVector.sub(other.getJointPosition(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance)
			{
				return false;
			}
		}
		return true;
	}
	
	public float difference(Body other)
	{
		PVector centerA = getJointPosition(JIDX.SpineBase);
		PVector centerB = other.getJointPosition(JIDX.SpineBase);
		float deviationMag = 0f;
		for (JIDX jidx : JIDX.values())
		{
			if (JIDX.SpineBase == jidx)
				continue;
			PVector radialA = PVector.sub(getJointPosition(jidx), centerA);
			PVector radialB = PVector.sub(other.getJointPosition(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			deviationMag += Math.abs(deviation.mag());
		}
		return deviationMag;
	}

	public float characteristicSize()
	{
		float sumsize = 0.0f;

		for (Pair<JIDX, JIDX> jidxpair : LIDX.LIMB_ENDS().values())
		{
			sumsize += PVector.sub(getJointPosition(jidxpair.second), getJointPosition(jidxpair.first)).mag();
		}

		return sumsize / LIDX.LIMB_ENDS().size();
	}

	public Body scale(float factor)
	{
		Body scaled = new Body();

		for (JIDX jidx : JIDX.values())
		{
			scaled.setJointPosition(jidx, PVector.mult(getJointPosition(jidx), factor));
		}
		
		return scaled;
	}
	
	public String toString()
	{
		String text = "[Body: [Joints: [";
		JIDX[] jointIndices = JIDX.values();
		text += "" + jointIndices[0].toString() + ": " + getJointPosition(jointIndices[0]).toString();
		for(int index = 1; index < jointIndices.length; index++)
		{
			text += ", " + jointIndices[index].toString() + ": " + getJointPosition(jointIndices[index]).toString();
		}
		
		text += "]]]";
		return text;
	}
	
	//TODO: Change to a method that returns the average size of all limbs.
	public static Body getIdealUserPose()
	{
		Body idealUserPose = new Body();
		//Set Skeleton Coordinates for ideal user Body
		idealUserPose.setJointPosition(JIDX.Head,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.setJointPosition(JIDX.Neck,new PVector(49.31178f, 482.05545f, 1871.1399f));
		idealUserPose.setJointPosition(JIDX.ShoulderLeft,new PVector(-117.19889f, 473.39166f, 1895.8171f));
		idealUserPose.setJointPosition(JIDX.ElbowLeft,new PVector(-234.26807f, 214.76712f, 1940.3701f));
		idealUserPose.setJointPosition(JIDX.HandLeft,new PVector(-502.1734f, 154.27269f, 1752.4586f));
		idealUserPose.setJointPosition(JIDX.HandTipLeft,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.setJointPosition(JIDX.ShoulderRight,new PVector(215.82245f, 490.7192f, 1846.4626f));
		idealUserPose.setJointPosition(JIDX.ElbowRight,new PVector(380.55743f, 234.44539f, 1845.75f));
		idealUserPose.setJointPosition(JIDX.HandRight,new PVector(641.58655f, 210.31554f, 1670.9052f));
		idealUserPose.setJointPosition(JIDX.HandTipRight,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.setJointPosition(JIDX.SpineBase,new PVector(56.47932f, 240.32893f, 1834.6368f));
		idealUserPose.setJointPosition(JIDX.HipLeft,new PVector(-43.330074f, -6.9635806f, 1813.9879f));
		idealUserPose.setJointPosition(JIDX.KneeLeft,new PVector(-31.571592f, -493.167f, 1824.4634f));
		idealUserPose.setJointPosition(JIDX.AnkleLeft,new PVector(18.669317f, -967.7855f, 1913.0208f));
		idealUserPose.setJointPosition(JIDX.HipRight,new PVector(170.6238f, 4.1684284f, 1782.2798f));
		idealUserPose.setJointPosition(JIDX.KneeRight,new PVector(193.66336f, -464.59735f, 1739.4097f));
		idealUserPose.setJointPosition(JIDX.AnkleRight,new PVector(221.68254f, -936.3788f, 1838.4629f));
		idealUserPose = idealUserPose.translated(new PVector(0, 0, idealUserPose.center().z));
		return idealUserPose;
	}

	public PVector getJointPosition(JIDX jidx)
	{
		return this.jointPositions.get(jidx);
	}

	public void setJointPosition(JIDX jpidx, PVector vector)
	{
		this.jointPositions.put(jpidx, vector);
	}
	
	public Quaternion getJointAngle(JIDX iJaidx)
	{
		return this.jointAngles.get(iJaidx);
	}
	
	public void setJointAngle(JIDX iJaidx, Quaternion iQuaternion)
	{
		this.jointAngles.put(iJaidx, iQuaternion);
	}

	/**
	 * @return the jointPositions
	 */
	public HashMap<JIDX, PVector> getJointPositions()
	{
		return jointPositions;
	}

	/**
	 * @param jointPositions the jointPositions to set
	 */
	public void setJointPositions(HashMap<JIDX, PVector> jointPositions)
	{
		this.jointPositions = jointPositions;
	}

	/**
	 * @return the jointAngles
	 */
	public HashMap<JIDX, Quaternion> getJointAngles()
	{
		return jointAngles;
	}

	/**
	 * @param jointAngles the jointAngles to set
	 */
	public void setJointAngles(HashMap<JIDX, Quaternion> jointAngles)
	{
		this.jointAngles = jointAngles;
	}

	/**
	 * @return the anchorJoint
	 */
	public JIDX getAnchorJoint()
	{
		return anchorJoint;
	}

	/**
	 * @param anchorJoint the anchorJoint to set
	 */
	public void setAnchorJoint(JIDX anchorJoint)
	{
		this.anchorJoint = anchorJoint;
	}
}
