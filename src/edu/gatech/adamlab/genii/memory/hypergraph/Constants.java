package edu.gatech.adamlab.genii.memory.hypergraph;

import java.util.HashMap;

import edu.gatech.adamlab.genii.shared.Pair;

/**
 * @author mikhailjacob
 *
 */
public class Constants
{
	public enum LinkType
	{
		NEXT_JOINT,
		PREVIOUS_JOINT,
		NEXT_LIMB,
		PREVIOUS_LIMB
	}
	
	public enum LimbType
	{
		HEAD,
		TORSO,
		LEFT_ARM,
		RIGHT_ARM,
		LEFT_LEG,
		RIGHT_LEG;
		
		public static JIDX[] getJointIndices(LimbType type)
		{
			if(type == HEAD)
			{
				return new JIDX[] {JIDX.Head, JIDX.Neck, JIDX.SpineShoulder};
			}
			else if(type == TORSO)
			{
				return new JIDX[] {JIDX.SpineBase, JIDX.SpineMid, JIDX.SpineShoulder, JIDX.ShoulderLeft, JIDX.ShoulderRight, JIDX.HipLeft, JIDX.HipRight};
			}
			else if(type == LEFT_ARM)
			{
				return new JIDX[] {JIDX.ShoulderLeft, JIDX.ElbowLeft, JIDX.WristLeft, JIDX.HandLeft, JIDX.HandTipLeft, JIDX.ThumbLeft};
			}
			else if(type == RIGHT_ARM)
			{
				return new JIDX[] {JIDX.ShoulderRight, JIDX.ElbowRight, JIDX.WristRight, JIDX.HandRight, JIDX.HandTipRight, JIDX.ThumbRight};
			}
			else if(type == LEFT_LEG)
			{
				return new JIDX[] {JIDX.HipLeft, JIDX.KneeLeft, JIDX.AnkleLeft, JIDX.FootLeft};
			}
			else if(type == RIGHT_LEG)
			{
				return new JIDX[] {JIDX.HipRight, JIDX.KneeRight, JIDX.AnkleRight, JIDX.FootRight};
			}
			
			return null;
		}
	}
	
	public enum JIDX
	{
		SpineBase, //Base of the spine
		SpineMid, //Middle of the spine
		Neck, //Neck
		Head, //Head
		ShoulderLeft, //Left shoulder
		ElbowLeft, //Left elbow
		WristLeft, //Left wrist
		HandLeft, //Left hand
		ShoulderRight, //Right shoulder
		ElbowRight, //Right elbow
		WristRight, //Right wrist;
		HandRight, //Right hand
		HipLeft, //Left hip
		KneeLeft, //Left knee
		AnkleLeft, //Left ankle
		FootLeft, //Left foot
		HipRight, //Right hip
		KneeRight, //Right knee
		AnkleRight, //Right ankle
		FootRight, //Right foot
		SpineShoulder, //Spine at the shoulder
		HandTipLeft, //Tip of the left hand
		ThumbLeft, //Left thumb
		HandTipRight, //Tip of the right hand
		ThumbRight //Right thumb
	}
	
//	public enum JAIDX
//	{
//		HEAD, NECK, 
//	    LEFT_COLLAR, LEFT_SHOULDER, LEFT_ELBOW, LEFT_HAND, LEFT_FINGERTIP, 
//	    RIGHT_COLLAR, RIGHT_SHOULDER, RIGHT_ELBOW, RIGHT_HAND, RIGHT_FINGERTIP, 
//	    TORSO, WAIST, 
//	    LEFT_HIP, LEFT_KNEE, LEFT_ANKLE, LEFT_FOOT, 
//	    RIGHT_HIP, RIGHT_KNEE, RIGHT_ANKLE, RIGHT_FOOT;
//	}
	
//	public enum JIDX // OLD OPENNI / KINECT 1 JIDX
//	{
//		HEAD, NECK, 
//	    LEFT_COLLAR, LEFT_SHOULDER, LEFT_ELBOW, LEFT_HAND, LEFT_FINGERTIP, 
//	    RIGHT_COLLAR, RIGHT_SHOULDER, RIGHT_ELBOW, RIGHT_HAND, RIGHT_FINGERTIP, 
//	    TORSO, WAIST, 
//	    LEFT_HIP, LEFT_KNEE, LEFT_ANKLE, LEFT_FOOT, 
//	    RIGHT_HIP, RIGHT_KNEE, RIGHT_ANKLE, RIGHT_FOOT;
//	}
	
	public enum LIDX
	{
		LowerTorso, UpperTorso, Neck, Head,
		CollarLeft, UpperArmLeft, ForeArmLeft, HandLeft, FingersLeft, ThumbLeft,
		CollarRight, UpperArmRight, ForeArmRight, HandRight, FingersRight, ThumbRight,
		PelvisLeft, ThighLeft, ShinLeft, FootLeft,
		PelvisRight, ThighRight, ShinRight, FootRight;

		public static HashMap<LIDX, Pair<JIDX, JIDX>> _LIMB_ENDS = null;

		public static HashMap<LIDX, Pair<JIDX, JIDX>> LIMB_ENDS()
		{
			if (null != _LIMB_ENDS)
				return _LIMB_ENDS;

			_LIMB_ENDS = new HashMap<LIDX, Pair<JIDX, JIDX>>();
			_LIMB_ENDS.put(LIDX.LowerTorso, Pair.of(JIDX.SpineBase, JIDX.SpineMid));
			_LIMB_ENDS.put(LIDX.UpperTorso, Pair.of(JIDX.SpineMid, JIDX.SpineShoulder));
			_LIMB_ENDS.put(LIDX.Neck, Pair.of(JIDX.SpineShoulder, JIDX.Neck));
			_LIMB_ENDS.put(LIDX.Head, Pair.of(JIDX.Neck, JIDX.Head));
			_LIMB_ENDS.put(LIDX.CollarLeft, Pair.of(JIDX.SpineShoulder, JIDX.ShoulderLeft));
			_LIMB_ENDS.put(LIDX.UpperArmLeft, Pair.of(JIDX.ShoulderLeft, JIDX.ElbowLeft));
			_LIMB_ENDS.put(LIDX.ForeArmLeft, Pair.of(JIDX.ElbowLeft, JIDX.WristLeft));
			_LIMB_ENDS.put(LIDX.HandLeft, Pair.of(JIDX.WristLeft, JIDX.HandLeft));
			_LIMB_ENDS.put(LIDX.FingersLeft, Pair.of(JIDX.HandLeft, JIDX.HandTipLeft));
			_LIMB_ENDS.put(LIDX.ThumbLeft, Pair.of(JIDX.HandLeft, JIDX.ThumbLeft));
			_LIMB_ENDS.put(LIDX.CollarRight, Pair.of(JIDX.SpineShoulder, JIDX.ShoulderRight));
			_LIMB_ENDS.put(LIDX.UpperArmRight, Pair.of(JIDX.ShoulderRight, JIDX.ElbowRight));
			_LIMB_ENDS.put(LIDX.ForeArmRight, Pair.of(JIDX.ElbowRight, JIDX.WristRight));
			_LIMB_ENDS.put(LIDX.HandRight, Pair.of(JIDX.WristRight, JIDX.HandRight));
			_LIMB_ENDS.put(LIDX.FingersRight, Pair.of(JIDX.HandRight, JIDX.HandTipRight));
			_LIMB_ENDS.put(LIDX.ThumbRight, Pair.of(JIDX.HandRight, JIDX.ThumbRight));
			_LIMB_ENDS.put(LIDX.PelvisLeft, Pair.of(JIDX.SpineBase, JIDX.HipLeft));
			_LIMB_ENDS.put(LIDX.ThighLeft, Pair.of(JIDX.HipLeft, JIDX.KneeLeft));
			_LIMB_ENDS.put(LIDX.ShinLeft, Pair.of(JIDX.KneeLeft, JIDX.AnkleLeft));
			_LIMB_ENDS.put(LIDX.FootLeft, Pair.of(JIDX.AnkleLeft, JIDX.FootLeft));
			_LIMB_ENDS.put(LIDX.PelvisRight, Pair.of(JIDX.SpineBase, JIDX.HipRight));
			_LIMB_ENDS.put(LIDX.ThighRight, Pair.of(JIDX.HipRight, JIDX.KneeRight));
			_LIMB_ENDS.put(LIDX.ShinRight, Pair.of(JIDX.KneeRight, JIDX.AnkleRight));
			_LIMB_ENDS.put(LIDX.FootRight, Pair.of(JIDX.AnkleRight, JIDX.FootRight));
			return _LIMB_ENDS;
		}
	}
}