/**
 * 
 */
package edu.gatech.adamlab.genii.memory.hypergraph.atoms;

import java.util.HashMap;
import java.util.UUID;

import edu.gatech.adamlab.genii.io.osc.statedefinition.ObjectStatePercept;

/**
 * @author mikhailjacob
 *
 */
public class OpenWorldState
{
	/**
	 * Hashmap of uuids to object state percepts. Maps an object id to its state. 
	 */
	private HashMap<UUID, ObjectStatePercept> worldState;
	
	public OpenWorldState()
	{
		this.worldState = new HashMap<UUID, ObjectStatePercept>();
	}
	
	public OpenWorldState(HashMap<UUID, ObjectStatePercept> worldState)
	{
		this.worldState = worldState;
	}

	/**
	 * @return the worldState
	 */
	public HashMap<UUID, ObjectStatePercept> getWorldState()
	{
		return worldState;
	}

	/**
	 * @param worldState the worldState to set
	 */
	public void setWorldState(HashMap<UUID, ObjectStatePercept> worldState)
	{
		this.worldState = worldState;
	}
	
	public ObjectStatePercept getObjectStatePercept(UUID id)
	{
		return worldState.get(id);
	}
	
	public void setObjectStatePercept(UUID id, ObjectStatePercept objectStatePercept)
	{
		worldState.put(id, objectStatePercept);
	}
}
