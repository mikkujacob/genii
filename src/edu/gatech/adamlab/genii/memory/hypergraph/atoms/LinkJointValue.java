/**
 * 
 */
package edu.gatech.adamlab.genii.memory.hypergraph.atoms;

import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;
import edu.gatech.adamlab.genii.memory.hypergraph.Constants.LinkType;

/**
 * @author mikhailjacob
 *
 */
public class LinkJointValue
{
	/**
	 * The type of link represented.
	 */
	private LinkType type;
	
	/**
	 * The joint index represented by the link.
	 */
	private JIDX jointIndex;
	
	/**
	 * Default constructor to create a Java Bean
	 */
	public LinkJointValue() {}
	
	/**
	 * Public parameterized constructor for this class.
	 * @param type
	 * @param jointIndex
	 * @param targetJointIndex
	 */
	public LinkJointValue(LinkType type, JIDX jointIndex)
	{
		this.type = type;
		this.jointIndex = jointIndex; 
	}

	/**
	 * @return the type
	 */
	public LinkType getType()
	{
		return type;
	}

	/**
	 * @return the jointIndex
	 */
	public JIDX getJointIndex()
	{
		return jointIndex;
	}
}
