/**
 * 
 */
package edu.gatech.adamlab.genii.memory.hypergraph.atoms;

import java.text.DecimalFormat;

import edu.gatech.adamlab.genii.shared.PVector;

/**
 * @author mikhailjacob
 *
 */
public class JointPosition implements Comparable<JointPosition>
{
	public static final float DEFAULT_EPSILON = 10f;
	public static final String DEFAULT_PRECISION = "#";
	private static final DecimalFormat DEFAULT_PRECISION_FORMAT = new DecimalFormat(DEFAULT_PRECISION);
	
	private float ex;
	private float ey;
	private float ez;
	
	private float x;
	private float y;
	private float z;

	private float epsilon = DEFAULT_EPSILON;

	public JointPosition()
	{
		
	}

	public JointPosition(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
		}
	}

	public JointPosition(float x, float y, float z, float epsilon)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
		}
	}

	public JointPosition(PVector p)
	{
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
		}
	}

	public JointPosition(PVector p, float epsilon)
	{
		this.x = p.x;
		this.y = p.y;
		this.z = p.z;
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
		}
	}

	public PVector getJointPosition()
	{
		return new PVector(x, y, z);
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
		}
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
		
		if(this.epsilon > 0)
		{
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
		}
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
		
		if(this.epsilon > 0)
		{
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
		}
	}

	public float getEpsilon()
	{
		return epsilon;
	}

	public void setEpsilon(float epsilon)
	{
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
		}
	}

	@Override
	/**
	 * If reduced precision dimensions are equal then they are equal.
	 * Otherwise Manhattan Distance of the two entities is used to compare them.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(JointPosition o)
	{
		if (o == this)
		{
			return 0;
		}
		
		if(this.ex == o.ex && this.ey == o.ey && this.ez == o.ez)
		{
			return 0;
		}
		else if((this.ex - o.ex + this.ey - o.ey + this.ez - o.ez) < 0)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}

	@Override
	/**
	 * Converts the object to a String representation.
	 */
	public String toString()
	{
		return "(" + DEFAULT_PRECISION_FORMAT.format(this.ex) + ", " + DEFAULT_PRECISION_FORMAT.format(this.ey) + ", " + DEFAULT_PRECISION_FORMAT.format(this.ez) + ", " + this.epsilon + ")";
	}

	@Override
	/**
	 * Uses the String hashCode() method after converting the object to a String.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		return this.toString().hashCode();
	}

	@Override
	/**
	 * If reduced precision dimensions are equal then they are equal.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o)
	{
		if (o == this)
		{
			return true;
		}

		if (!(o instanceof JointPosition))
		{
			return false;
		}

		JointPosition otherPos = (JointPosition) o;
		
		if(this.ex == otherPos.ex && this.ey == otherPos.ey && this.ez == otherPos.ez)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
