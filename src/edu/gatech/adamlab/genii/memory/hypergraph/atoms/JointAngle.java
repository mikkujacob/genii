/**
 * 
 */
package edu.gatech.adamlab.genii.memory.hypergraph.atoms;

import java.text.DecimalFormat;

import edu.gatech.adamlab.genii.shared.Quaternion;

/**
 * @author mikhailjacob
 *
 */
public class JointAngle implements Comparable<JointAngle>
{
	public static final float DEFAULT_EPSILON = 10f;
	public static final String DEFAULT_PRECISION = "#";
	private static final DecimalFormat DEFAULT_PRECISION_FORMAT = new DecimalFormat(DEFAULT_PRECISION);
	
	private float ex;
	private float ey;
	private float ez;
	private float ew;
	
	private float x;
	private float y;
	private float z;
	private float w;

	private float epsilon = DEFAULT_EPSILON;

	public JointAngle()
	{
		
	}

	public JointAngle(float x, float y, float z, float w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
			this.ew = this.w / this.epsilon;
		}
	}

	public JointAngle(float x, float y, float z, float w, float epsilon)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
			this.ew = this.w / this.epsilon;
		}
	}

	public JointAngle(Quaternion q)
	{
		this.x = q.x();
		this.y = q.y();
		this.z = q.z();
		this.w = q.w();
		
		if(this.epsilon > 0)
		{
			this.ex = this.x / this.epsilon;
			this.ey = this.y / this.epsilon;
			this.ez = this.z / this.epsilon;
			this.ew = this.w / this.epsilon;
		}
	}

	public JointAngle(Quaternion q, float epsilon)
	{
		this.x = q.x();
		this.y = q.y();
		this.z = q.z();
		this.w = q.w();
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
			this.ew = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.w / this.epsilon));
		}
	}

	public Quaternion getJointPosition()
	{
		return new Quaternion(x, y, z, w);
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
		}
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
		
		if(this.epsilon > 0)
		{
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
		}
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
		
		if(this.epsilon > 0)
		{
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
		}
	}

	public float getW()
	{
		return w;
	}

	public void setW(float w)
	{
		this.w = w;
		
		if(this.epsilon > 0)
		{
			this.ew = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.w / this.epsilon));
		}
	}

	public float getEpsilon()
	{
		return epsilon;
	}

	public void setEpsilon(float epsilon)
	{
		this.epsilon = epsilon;
		
		if(this.epsilon > 0)
		{
			this.ex = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.x / this.epsilon));
			this.ey = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.y / this.epsilon));
			this.ez = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.z / this.epsilon));
			this.ew = Float.parseFloat(DEFAULT_PRECISION_FORMAT.format(this.w / this.epsilon));
		}
	}

	@Override
	/**
	 * If reduced precision dimensions are equal then they are equal.
	 * Otherwise Manhattan Distance of the two entities is used to compare them.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(JointAngle o)
	{
		if (o == this)
		{
			return 0;
		}
		
		if(this.ex == o.ex && this.ey == o.ey && this.ez == o.ez && this.ew == o.ew)
		{
			return 0;
		}
		else if((this.ex - o.ex + this.ey - o.ey + this.ez - o.ez + this.ew - o.ew) < 0)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}

	@Override
	/**
	 * Converts the object to a String representation.
	 */
	public String toString()
	{
		return "(" + DEFAULT_PRECISION_FORMAT.format(this.ex) + ", " + DEFAULT_PRECISION_FORMAT.format(this.ey) + ", " + DEFAULT_PRECISION_FORMAT.format(this.ez) + ", " + DEFAULT_PRECISION_FORMAT.format(this.ew) + ", " + this.epsilon + ")";
	}

	@Override
	/**
	 * Uses the String hashCode() method after converting the object to a String.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		return this.toString().hashCode();
	}

	@Override
	/**
	 * If reduced precision dimensions are equal then they are equal.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o)
	{
		if (o == this)
		{
			return true;
		}

		if (!(o instanceof JointAngle))
		{
			return false;
		}

		JointAngle otherPos = (JointAngle) o;
		
		if(this.ex == otherPos.ex && this.ey == otherPos.ey && this.ez == otherPos.ez && this.ew == otherPos.ew)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
