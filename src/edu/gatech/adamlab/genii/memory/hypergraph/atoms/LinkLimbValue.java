/**
 * 
 */
package edu.gatech.adamlab.genii.memory.hypergraph.atoms;

import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;
import edu.gatech.adamlab.genii.memory.hypergraph.Constants.LinkType;

/**
 * @author mikhailjacob
 *
 */
public class LinkLimbValue
{
	/**
	 * Type of link represented.
	 */
	private LinkType type;
	
	/**
	 * Joint indices represented by the link.
	 */
	private JIDX[] jointIndices;
	
	/**
	 * Default constructor to create a Java Bean
	 */
	public LinkLimbValue() {}
	
	/**
	 * Public parameterized constructor for this class.
	 * @param type
	 * @param jointIndices
	 */
	public LinkLimbValue(LinkType type, JIDX[] jointIndices)
	{
		this.setType(type);
		this.setJointIndices(jointIndices);
	}

	/**
	 * @return the type
	 */
	public LinkType getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(LinkType type)
	{
		this.type = type;
	}

	/**
	 * @return the jointIndices
	 */
	public JIDX[] getJointIndices()
	{
		return jointIndices;
	}

	/**
	 * @param jointIndices the jointIndices to set
	 */
	public void setJointIndices(JIDX[] jointIndices)
	{
		this.jointIndices = jointIndices;
	}
}
