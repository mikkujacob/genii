package edu.gatech.adamlab.genii.memory.hypergraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.hypergraphdb.HGConfiguration;
import org.hypergraphdb.HGEnvironment;
import org.hypergraphdb.HGHandle;
import org.hypergraphdb.HyperGraph;
import org.hypergraphdb.algorithms.DefaultALGenerator;
import org.hypergraphdb.algorithms.HGBreadthFirstTraversal;
import org.hypergraphdb.algorithms.HGDepthFirstTraversal;
import org.hypergraphdb.algorithms.HGTraversal;
import org.hypergraphdb.algorithms.SimpleALGenerator;
import org.hypergraphdb.query.ComparisonOperator;
import org.hypergraphdb.util.Pair;
import org.hypergraphdb.HGQuery.hg;
import org.hypergraphdb.HGValueLink;

import edu.gatech.adamlab.genii.io.file.Logger;
import edu.gatech.adamlab.genii.io.osc.statedefinition.KinectStatePercept;
import edu.gatech.adamlab.genii.io.osc.statedefinition.ObjectStatePercept;
import edu.gatech.adamlab.genii.io.socket.perception.PerceptionSocketOutputData;
import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;
import edu.gatech.adamlab.genii.memory.hypergraph.Constants.LimbType;
import edu.gatech.adamlab.genii.memory.hypergraph.Constants.LinkType;
import edu.gatech.adamlab.genii.memory.hypergraph.atoms.JointPosition;
import edu.gatech.adamlab.genii.memory.hypergraph.atoms.LinkJointValue;
import edu.gatech.adamlab.genii.memory.hypergraph.atoms.LinkLimbValue;
import edu.gatech.adamlab.genii.memory.hypergraph.atoms.OpenWorldState;
import edu.gatech.adamlab.genii.shared.Body;

public class HyperGraphMemoryModel
{
	/**
	 * String representing the location of the database on disk.
	 */
	private final static String databaseLocation = "data/hgdb/";
	
	/**
	 * The hypergraph object itself.
	 */
	private HyperGraph graph;
	
	/**
	 * HashMap of joint indices and their joint positions for the current percept.
	 */
	private HashMap<JIDX, HGHandle> currentJointHandleMap = new HashMap<JIDX, HGHandle>();
	
	/**
	 * HashMap of joint indices and their joint positions previous percept.
	 */
	private HashMap<JIDX, HGHandle> previousJointHandleMap = new HashMap<JIDX, HGHandle>();
	
	/**
	 * Frames of Body position for the duration of a single action.
	 */
	private ArrayList<Body> movementFrames;
	
	/**
	 * Indices for movement frames that segment the movement in sub-actions.
	 */
	private ArrayList<Integer> keyMovementFrameIndices;
	
	/**
	 * Map between key frames of movement to the list of ObjectStatePercept objects that correspond to changes in world state due to the observed movement.
	 */
	private HashMap<Integer, ArrayList<ObjectStatePercept>>movementFrameIndexToObjectStatePerceptList;
	
	/**
	 * Map between UUIDs and initial set of ObjectStatePercept objects that correspond to changes in world state due to the observed movement.
	 */
	private HashMap<UUID, ObjectStatePercept>initialUUIDToObjectStatePercept;
	
	/**
	 * Map between UUIDs and current set of ObjectStatePercept objects that correspond to changes in world state due to the observed movement.
	 */
	private HashMap<UUID, ObjectStatePercept>currentUUIDToObjectStatePercept;
	
	/**
	 * Map between UUIDs and final set of ObjectStatePercept objects that correspond to changes in world state due to the observed movement.
	 */
	private HashMap<UUID, ObjectStatePercept>finalUUIDToObjectStatePercept;
	
	/**
	 * Temporary summary evaluation statistics
	 */
	public int hits, misses, total;
	
//	/**
//	 * Main method to run and test the class.
//	 * @param args
//	 */
//	public static void main(String[] args)
//	{
//		HyperGraphMemoryModel memoryModel = new HyperGraphMemoryModel();
//		Logger.log("\nHyperGraph Memory Model:\t" + memoryModel.toString());
//	}
	
	/**
	 * The default constructor.
	 */
	public HyperGraphMemoryModel()
	{
		try
		{
			initializeGraph();
			initializeActionStateTrackingVariables();
			
//			//Add some atoms to hypergraph
//			HGHandle handle1 = graph.add("This is a String object");
//			HGHandle handle1 = graph.add((double)1.222);
//			HGHandle handle2 = graph.add((double)5.222);
//			HGHandle handle3 = graph.add((double)0.222);
//			HGHandle handle4 = graph.add((double)10.222);
//			HGHandle handle5 = graph.add((double)13.222);
//			HGHandle handle1 = graph.add(new JointPosition(0,0,0, 100));
//			HGHandle handle2 = graph.add(new JointPosition(1,1,1, 100));
//			HGHandle handle3 = graph.add(new JointPosition(2,2,2, 100));
//			HGHandle handle4 = graph.add(new JointPosition(5,5,5, 100));
//			HGHandle handle5 = graph.add(new JointPosition(10,10,10, 100));
//			HGHandle handle3 = graph.add((int)99);
			
//			 HGHandle result = getNearestAtom(5.5, Double.class);
//			 HGHandle result = getNearestAtom(new JointPosition(0,10,10, 100), JointPosition.class);
			 
//			 if(result != null)
//			 {
//				 Logger.log("FOUND RESULTS");
//				 printHyperGraph(result, false, true, true);
//			 }
//			 else
//			 {
//				 Logger.log("FOUND NO RESULTS");
//			 }
//			
//			//Create some links
//			HGValueLink link1 = new HGValueLink("String2Double", handle1, handle2);
//			HGValueLink link2 = new HGValueLink("Double2Integer", handle2, handle3);
//			HGValueLink link3 = new HGValueLink("Integer2String", handle3, handle1);
//			
//			//Add some links to hypergraph
//			HGHandle handle4 = graph.add(link1);
//			HGHandle handle5 = graph.add(link2);
//			HGHandle handle6 = graph.add(link3);
//			
//			//Create some hyperlinks
//			HGValueLink link4 = new HGValueLink("String2(String2Double)", handle1, handle4);
//			HGValueLink link5 = new HGValueLink("Double2(Double2Integer)", handle2, handle5);
//			HGValueLink link6 = new HGValueLink("Integer2(Integer2String)", handle3, handle6);
//			
//			//Add some hyperlinks to hypergraph
//			HGHandle handle7 = graph.add(link4);
//			HGHandle handle8 = graph.add(link5);
//			HGHandle handle9 = graph.add(link6);
//			
//			//Create one more hyperlink
//			HGValueLink link7 = new HGValueLink("(String2(String2Double))2(Double2(Double2Integer))2(Integer2(Integer2String))", handle7, handle8, handle9);
//			
//			//Add one more hyperlink to hypergraph
//			HGHandle handle10 = graph.add(link7);
//			
//			//Create one more hyperlink
//			HGValueLink link8 = new HGValueLink("String2Double2Integer2((String2(String2Double))2(Double2(Double2Integer))2(Integer2(Integer2String)))", handle1, handle2, handle3, handle10);
//			
//			//Add one more hyperlink to hypergraph
//			graph.add(link8);
//			
//			//Print hypergraph DFS
//			Logger.log("HyperGraphDB Contents (DFS, SimpleALGen):\n");
//			printHyperGraph(handle1, false, true, true);
//			
//			//Print hypergraph DFS
//			Logger.log("HyperGraphDB Contents (DFS, DefaultALGen):\n");
//			printHyperGraph(handle1, false, true, false);
//			
//			//Print hypergraph BFS
//			Logger.log("HyperGraphDB Contents (BFS, SimpleALGen):\n");
//			printHyperGraph(handle1, false, false, true);
//			
//			//Print hypergraph BFS
//			Logger.log("HyperGraphDB Contents (BFS, DefaultALGen):\n");
//			printHyperGraph(handle1, false, false, false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize the graph and associated properties of it.
	 */
	public void initializeGraph()
	{
		//Initialize hypergraph
		HGConfiguration config = new HGConfiguration();
		config.setTransactional(false);
		graph = HGEnvironment.get(databaseLocation, config);
	}
	
	/**
	 * Initialize action state tracking variables.
	 */
	public void initializeActionStateTrackingVariables()
	{
		currentJointHandleMap = new HashMap<JIDX, HGHandle>();
		previousJointHandleMap = new HashMap<JIDX, HGHandle>();
		movementFrames = new ArrayList<Body>();
		keyMovementFrameIndices = new ArrayList<Integer>();
		movementFrameIndexToObjectStatePerceptList = new HashMap<Integer, ArrayList<ObjectStatePercept>>();
		initialUUIDToObjectStatePercept = new HashMap<UUID, ObjectStatePercept>();
		currentUUIDToObjectStatePercept = new HashMap<UUID, ObjectStatePercept>();
		finalUUIDToObjectStatePercept = new HashMap<UUID, ObjectStatePercept>();
	}
	
	/**
	 * Reset action state tracking variables.
	 */
	public void resetActionStateTrackingVariables()
	{
		movementFrames.clear();
		keyMovementFrameIndices.clear();
		movementFrameIndexToObjectStatePerceptList.clear();
		initialUUIDToObjectStatePercept.clear();
		currentUUIDToObjectStatePercept.clear();
		finalUUIDToObjectStatePercept.clear();
	}
	
	/**
	 * Method to close the graph and free up resources from the OS.
	 */
	public void closeGraph()
	{
		try
		{
			if(graph.isOpen())
			{
				graph.close();
			}
		}
		catch (RuntimeException e)
		{
			Logger.log("RuntimeException while closing hypergraph");
			Logger.log(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Interpret start action percept
	 * @param isEnd - is it the end of the start action percept?
	 */
	public void interpretStartActionPercept(boolean isEnd)
	{
		Logger.log("Interpreting Start Action");
		//Starting action command
		//Get all object state tracking states for initial states
		//Starting action Body frame
		//Sequence of Body frames between two consecutive object state tracking states
		//Sequence of consecutive object state tracking states  is an action
		//An action
		//Ending action command
		//Get all object state tracking states for final states
	}
	
	/**
	 * Interpret start action percept
	 * @param objectState - the start state object state percept to interpret
	 */
	public void interpretStartActionPercept(ObjectStatePercept objectState)
	{
		initialUUIDToObjectStatePercept.put(objectState.getUuid(), objectState);
		Logger.log("Interpreting Start Action Initial Object State");
		//Starting action command
		//Starting action Body frame
		//Sequence of Body frames between two consecutive object state tracking states
		//Sequence of consecutive object state tracking states  is an action
		//An action
		//Ending action command
		//Get all object state tracking states for final states
	}
	
	/**
	 * Interpret end action percept
	 * @param isEnd - is it the end of the end action percept?
	 */
	public void interpretEndActionPercept(boolean isEnd)
	{
		if(isEnd)
		{
			resetActionStateTrackingVariables();
		}
		
		Logger.log("Interpreting End Action");
		//Starting action command
		//Starting action Body frame
		//Sequence of Body frames between two consecutive object state tracking states
		//Sequence of consecutive object state tracking states  is an action
		//An action
		//Ending action command
		//Get all object state tracking states for final states
	}
	
	/**
	 * Interpret end action percept
	 * @param objectState - the end state object state percept to interpret
	 */
	public void interpretEndActionPercept(ObjectStatePercept objectState)
	{
		finalUUIDToObjectStatePercept.put(objectState.getUuid(), objectState);
		Logger.log("Interpreting End Action Final Object State");
		//Starting action command
		//Starting action Body frame
		//Sequence of Body frames between two consecutive object state tracking states
		//Sequence of consecutive object state tracking states  is an action
		//An action
		//Ending action command
		//Get all object state tracking states for final states
	}
		
	/**
	 * Interpret an object state percept arriving over the GENIIMImEOSCBridge
	 * @param kinectState - kinect state to interpret.
	 */
	public void interpretKinectStatePercept(KinectStatePercept kinectState)
	{
		Logger.log("Interpreting Kinect State Percept: " + kinectState.toString());
		interpretBodyPercept(kinectState.getBody());
	}
	
	/**
	 * Interpret an object state percept arriving over the GENIIMImEOSCBridge
	 * @param objectState - object state to interpret.
	 */
	public void interpretObjectStatePercept(ObjectStatePercept objectState)
	{
		currentUUIDToObjectStatePercept.put(objectState.getUuid(), objectState);
		Logger.log("Interpreting Object State Percept: " + objectState.toString());
		
		//If there aren't any object state percepts for that movement frame index 
		if(movementFrameIndexToObjectStatePerceptList.get(movementFrames.size() - 1) == null)
		{
			movementFrameIndexToObjectStatePerceptList.put(movementFrames.size() - 1, new ArrayList<ObjectStatePercept>());
		}
		
		ArrayList<ObjectStatePercept> stateList = movementFrameIndexToObjectStatePerceptList.get(movementFrames.size() - 1);
		stateList.add(objectState);
		movementFrameIndexToObjectStatePerceptList.put(movementFrames.size() - 1, stateList);
		
		//TODO Add to HyperGraph
		OpenWorldState worldState = new OpenWorldState();
		
		//TODO Create an OpenWorldState atom encapsulating every KinectStatePercept around the time of the world state change event (number of KinectStatePercepts depends on the size and type of window function around the world state change event)
		
		
		//TODO Create WorldStateChange next and previous value links connecting OpenWorldState atoms with link value being TrackedChange objects and sequence of KinectStatePercepts from end of previous world state change event to end of current world state change event
		
		
		//TODO Create a SubAction atom encapsulating the World State Change next and previous value links as well as the
		
		
		//TODO At end of action, process sequence of SubAction atoms and create next+previous SubAction value links
		
		
		//TODO At end of action, create an Action atom encapsulating entire sequence.
		
		
	}
	
	/**
	 * Interpret a percept arriving from the PerceptionInputSocket.
	 * @param percept - percept to interpret.
	 */
	public void interpretPerceptionInputSocketPercept(PerceptionSocketOutputData percept)
	{
		interpretBodyPercept(percept.getBody());
	}
	
	/**
	 * Interpret a Body object received from the PerceptionInputSocket.
	 * @param body - body to interpret.
	 */
	private void interpretBodyPercept(Body body)//TODO: Add Joint Angles to this.
	{
		long currentTimeMillis = System.currentTimeMillis();
		
		movementFrames.add(body);
		
		//First run or previousJointHandleMap became null somehow
		if(previousJointHandleMap == null)
		{
			previousJointHandleMap = new HashMap<JIDX, HGHandle>();
		}
		
		//First run or jointHandleMap became null somehow
		if(currentJointHandleMap == null)
		{
			currentJointHandleMap = new HashMap<JIDX, HGHandle>();
		}
		
		Logger.log("Current Joint Table initially contains: " + currentJointHandleMap.toString());
		Logger.log("Previous Joint Table initially contains: " + previousJointHandleMap.toString());
		
		addJointAtomsAndJointLinks(body);
		
		addLimbLinks();
		
		//Remaining joint processing logic...
		
		Logger.log("Elapsed Time: " + (((double)(System.currentTimeMillis() - currentTimeMillis)) / 1000.0) + " seconds");
		Logger.log("Current Joint Table contains: " + currentJointHandleMap.toString());
		Logger.log("Previous Joint Table contains: " + previousJointHandleMap.toString());
		
//		ArrayList<HGHandle> handles = (ArrayList<HGHandle>) this.graph.findAll(hg.type(JointPosition.class));
//		for(HGHandle jointHandle : handles)
//		{
//			printHyperGraph(jointHandle, false, false, true);
//		}
		
//		ArrayList<Object> resultObjects = (ArrayList<Object>) this.graph.getAll(hg.type(JointPosition.class));
//		ArrayList<JointPosition> results = new ArrayList<JointPosition>();
//		
//		for(Object object : resultObjects)
//		{
//			results.add((JointPosition)object);
//		}
//		
//		Collections.sort(results);
//		
//		for(JointPosition jointPosition : results)
//		{
//			Logger.log(jointPosition.toString());
//		}

		Logger.log("Total: " + total);
		Logger.log("Hits: " + hits + ", Hits %: " + (float) hits / (float) total * 100);
		Logger.log("Misses: " + misses + ", Misses %: " + (float) misses / (float) total * 100);
	}
	
	/**
	 * Add joints to the hypergraph and build next and previous links between joints of the same type over time.
	 * @param body - The Body percept containing joint information.
	 */
	private void addJointAtomsAndJointLinks(Body body)
	{
		Logger.log("Adding Joints and Building Joint Links");
		
		//For all joints in Body body
		for(JIDX jointIndex : JIDX.values())
		{
			if(currentJointHandleMap.containsKey(jointIndex) && (currentJointHandleMap.get(jointIndex) != null))
			{
				//Set previous joint of that type to current joint
				previousJointHandleMap.put(jointIndex, currentJointHandleMap.get(jointIndex));
			}
			
			//Get current joint
			float epsilon = 100f;
			JointPosition jointPosition = new JointPosition(body.getJointPosition(jointIndex), epsilon);
			
			Logger.log("Current " + jointIndex + " joint is at: " + jointPosition.toString());
			
			//Increment Total counter
			total++;
			
			//Get nearest joint's HGHandle within distance threshold to current joint
			HGHandle nearestJointHandle = getNearestAtom(jointPosition, JointPosition.class);
			
			//If nearest joint < threshold and nearest joint's HGHandle != null
			if(nearestJointHandle != null)
			{
				Logger.log("Got a nearest joint handle: " + nearestJointHandle.toString());
				
				//Add new HGHandle to temporary table of joint index and HGHandles
				currentJointHandleMap.put(jointIndex, nearestJointHandle);
				
				//Increment Hits counter
				hits++;
			}
			else //Else
			{
				try
				{
					//Add new joint to Hypergraph at depth 0
					nearestJointHandle = graph.add(jointPosition);
				}
				catch (RuntimeException e)
				{
					Logger.log("RuntimeException while adding joint position to hypergraph");
					Logger.log(e.getMessage());
					e.printStackTrace();
				}
				
				Logger.log("Did not get a nearest joint handle. New handle: " + nearestJointHandle.toString());
				
				//Add new HGHandle to temporary table of joint index and HGHandles
				currentJointHandleMap.put(jointIndex, nearestJointHandle);
				
				//Increment Misses counter
				misses++;
			}
			//If previous joint is not null
			if(previousJointHandleMap.containsKey(jointIndex) && (previousJointHandleMap.get(jointIndex) != null))
			{
				//Create a Next link from previous joint of that type to current joint
				HGValueLink nextJointLink = new HGValueLink(LinkType.NEXT_JOINT, previousJointHandleMap.get(jointIndex), currentJointHandleMap.get(jointIndex));
				nextJointLink.setValue(new LinkJointValue(LinkType.NEXT_JOINT, jointIndex));
				try
				{
					//Add link to hypergraph
					graph.add(nextJointLink);
				}
				catch (RuntimeException e)
				{
					Logger.log("RuntimeException while adding next joint value link to hypergraph");
					Logger.log(e.getMessage());
					e.printStackTrace();
				}
				//Create a Previous link from current joint of that type to previous joint
				HGValueLink previousJointLink = new HGValueLink(LinkType.PREVIOUS_JOINT, currentJointHandleMap.get(jointIndex), previousJointHandleMap.get(jointIndex));
				previousJointLink.setValue(new LinkJointValue(LinkType.PREVIOUS_JOINT, jointIndex));
				try
				{
					//Add link to hypergraph
					graph.add(previousJointLink);
				}
				catch (RuntimeException e)
				{
					Logger.log("RuntimeException while adding previous joint position to hypergraph");
					Logger.log(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private HGHandle[] getHandleArrayFromJointArray(HashMap<JIDX, HGHandle> jointToHandleMap, JIDX[] jointArray)
	{
		HGHandle[] handles = new HGHandle[jointArray.length];
		for(int index = 0; index < jointArray.length; index++)
		{
			handles[index] = jointToHandleMap.get(jointArray[index]);
		}
		return handles;
	}
	
	private HGHandle[] getLinkHandleArrayFromJointArrays(HashMap<JIDX, HGHandle> jointToHandleMapA, HashMap<JIDX, HGHandle> jointToHandleMapB, JIDX[] jointArray)
	{
		HGHandle[] handles = new HGHandle[jointArray.length * 2];
		for(int index = 0; index < jointArray.length; index++)
		{
			handles[index] = jointToHandleMapA.get(jointArray[index]);
		}
		for(int index = 0; index < jointArray.length; index++)
		{
			handles[jointArray.length + index] = jointToHandleMapB.get(jointArray[index]);
		}
		return handles;
	}
	
	/**
	 * Build the next and previous links between limbs of the same type containing several joints for each limb. 
	 */
	private void addLimbLinks()
	{
		Logger.log("Building limb links...");
		
		//If previous list of limb joint HGHandles is not null
		if(!previousJointHandleMap.isEmpty())
		{
			//For all HGHandles in temporary list of limb joint HGHandles. Optimized with loop unrolling for execution speed but more brittle as a result. Size of limb joints array needs to match in the linklimbvalue object creation.
			//Create a Next link from previous

			//HEAD
			Logger.log("Building HEAD links...");
			JIDX[] jointIndices = LimbType.getJointIndices(LimbType.HEAD);
			LinkLimbValue nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			LinkLimbValue previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			HGValueLink nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			HGValueLink previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}

			//TORSO
			Logger.log("Building TORSO links...");
			jointIndices = LimbType.getJointIndices(LimbType.TORSO);
			nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}

			//LEFT_ARM
			Logger.log("Building LEFT_ARM links...");
			jointIndices = LimbType.getJointIndices(LimbType.LEFT_ARM);
			nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}

			//RIGHT_ARM
			Logger.log("Building RIGHT_ARM links...");
			jointIndices = LimbType.getJointIndices(LimbType.RIGHT_ARM);
			nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}

			//LEFT_LEG
			Logger.log("Building LEFT_LEG links...");
			jointIndices = LimbType.getJointIndices(LimbType.LEFT_LEG);
			nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}

			//RIGHT_LEG
			Logger.log("Building RIGHT_LEG links...");
			jointIndices = LimbType.getJointIndices(LimbType.RIGHT_LEG);
			nextLinkLimbValue = new LinkLimbValue(LinkType.NEXT_LIMB, jointIndices);
			previousLinkLimbValue = new LinkLimbValue(LinkType.PREVIOUS_LIMB, jointIndices);
			nextLimbLink = new HGValueLink(nextLinkLimbValue, getLinkHandleArrayFromJointArrays(previousJointHandleMap, currentJointHandleMap, jointIndices));
			previousLimbLink = new HGValueLink(previousLinkLimbValue, getLinkHandleArrayFromJointArrays(currentJointHandleMap, previousJointHandleMap, jointIndices));
			try
			{
				graph.add(nextLimbLink);
				graph.add(previousLimbLink);
			}
			catch (RuntimeException e)
			{
				Logger.log("RuntimeException while adding next limb position to hypergraph");
				Logger.log(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Finds the first atom that is equal. For getting the nearest atom, the atomtype must 
	 * implement the Comparable interface and use a threshold / epsilon within equality / 
	 * compareTo() method definition. Then this method can return the first atom found that 
	 * is within that threshold. UPDATE: The atomtype must override equals() and hashCode().
	 * @param atom - The atom to find the nearest to.
	 * @param atomType - The class literal of the atom to be searched with.
	 * @return The first atom that is found or null if none are found.
	 */
	private <A> HGHandle getNearestAtom(A atom, Class<A> atomType)
	{
//		Logger.log("Graph open: " + this.graph.isOpen());
//		Logger.log("Graph empty: " + this.graph.count(hg.all()));
		try
		{
//			ArrayList<HGHandle> results = (ArrayList<HGHandle>) this.graph.findAll(hg.and(hg.type(atomType), hg.value(atom, ComparisonOperator.EQ)));
//			ArrayList<HGHandle> results = (ArrayList<HGHandle>) this.graph.findAll(hg.type(atomType));
			ArrayList<HGHandle> results = (ArrayList<HGHandle>) this.graph.findAll(hg.value(atom, ComparisonOperator.EQ));
			if(results != null && !results.isEmpty())
			{
				Logger.log("Number of matching atoms: " + results.size());
				return results.get(0);
			}
			else
			{
				Logger.log("No matching atoms.");
			}
			return null;
		}
		catch(RuntimeException e)
		{
//			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to print the hypergraph.
	 * @param root - The node to designate as starting or root.
	 * @param onlyPrintNewAtoms - Whether to only print new atoms or not.
	 * @param isDFSTraversal - Whether traversal is DFS or BFS.
	 * @param isSimpleAdjacency - Whether adjacency list is Simple or Default.
	 */
	public void printHyperGraph(HGHandle root, boolean onlyPrintNewAtoms, boolean isDFSTraversal, boolean isSimpleAdjacency)
	{
		Logger.log("Root: " + graph.get(root) + "\n");
		HGTraversal traversal;
		if(isDFSTraversal && isSimpleAdjacency)
		{
			traversal = new HGDepthFirstTraversal(root, new SimpleALGenerator(graph));
		}
		else if(isDFSTraversal && !isSimpleAdjacency)
		{
			traversal = new HGDepthFirstTraversal(root, new DefaultALGenerator(graph));
		}
		else if(!isDFSTraversal && isSimpleAdjacency)
		{
			traversal = new HGBreadthFirstTraversal(root, new SimpleALGenerator(graph));
		}
		else
		{
			traversal = new HGBreadthFirstTraversal(root, new DefaultALGenerator(graph));
		}
		
		while(traversal.hasNext())
		{
			Pair<HGHandle, HGHandle> nextPair = traversal.next();
			HGHandle link = nextPair.getFirst();
			HGHandle atom = nextPair.getSecond();
			if(!traversal.isVisited(atom) || !onlyPrintNewAtoms)
			{
				Logger.log("Next Link: " + graph.get(link).toString());
				Logger.log("Next Atom: " + graph.get(atom).toString());
			}
		}
		
		Logger.log();
	}
	
	/**
	 * Method to convert this object to a string.
	 */
	@Override
	public String toString()
	{
		return graph.getLocation();
	}
	
	/**
	 * Getter for the hypergraph object.
	 * @return The hypergraph object.
	 */
	public HyperGraph getGraph()
	{
		return graph;
	}

	/**
	 * Setter for the hypergraph object.
	 * @param graph - The desired hypergraph to set it to.
	 */
	public void setGraph(HyperGraph graph)
	{
		this.graph = graph;
	}
}
