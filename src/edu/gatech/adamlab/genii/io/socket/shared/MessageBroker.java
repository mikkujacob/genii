package edu.gatech.adamlab.genii.io.socket.shared;

import org.zeromq.ZMQ;

import edu.gatech.adamlab.genii.io.socket.shared.BasicSocket.TRANSPORT;
import edu.gatech.adamlab.genii.io.socket.shared.BasicSocket.TYPE;

/**
 * A message broker that can take two sockets and queue messages between them in order
 * to create a common connection point for dynamic discovery, load balancing, etc.
 * @author mikhail.jacob
 *
 */
public class MessageBroker
{
	/**
	 * The socket object that proxies the transmitting source
	 */
	private BasicSocket receiver;
	
	/**
	 * The socket object that proxies the receiving sink
	 */
	private BasicSocket forwarder;
	
	/**
	 * The address of the receiver
	 */
	private String receiverAddress;
	
	/**
	 * The address of the forwarder
	 */
	private String forwarderAddress;
	
	/**
	 * The default address for a receiver
	 */
	public static final String defaultReceiverAddress = "tcp://localhost:56789"; //Port from 49152 to 65535
	
	/**
	 * The default address for a forwarder
	 */
	public static final String defaultForwarderAddress = "tcp://localhost:56790"; //Port from 49152 to 65535
	
	/**
	 * The default socket type for the receiver
	 */
	private static final TYPE defaultReceiverSocketType = TYPE.XSUB;

	/**
	 * The default socket type for the forwarder
	 */
	private static final TYPE defaultForwarderSocketType = TYPE.XPUB;

	/**
	 * Should the default receiver bind
	 */
	private static final boolean defaultShouldReceiverBind = true;

	/**
	 * Should the default forwarder bind
	 */
	private static final boolean defaultShouldForwarderBind = true;
	
	/**
	 * The constructor of the MessageBroker class instantiates a MessageBroker object with default parameters.
	 */
	public MessageBroker()
	{
		this(defaultReceiverAddress, defaultReceiverSocketType, defaultShouldReceiverBind, defaultForwarderAddress, defaultForwarderSocketType, defaultShouldForwarderBind);
	}
	
	/**
	 * The constructor of the MessageBroker class instantiates a MessageBroker object.
	 * @param transport1 TRANSPORT enumerator used to choose transport protocol / method.
	 * @param host1 Local or remote host to connect or bind to.
	 * @param port1 The port to connect or bind to.
	 * @param socketType1 The type of socket created.
	 * @param shouldBind1 Whether to bind the socket to the address as opposed to connecting.
	 * @param transport2 TRANSPORT enumerator used to choose transport protocol / method.
	 * @param host2 Local or remote host to connect or bind to.
	 * @param port2 The port to connect or bind to.
	 * @param socketType2 The type of socket created.
	 * @param shouldBind2 Whether to bind the socket to the address as opposed to connecting.
	 */
	public MessageBroker(TRANSPORT transport1, String host1, int port1, TYPE socketType1, boolean shouldBind1,
			TRANSPORT transport2, String host2, int port2, TYPE socketType2, boolean shouldBind2)
	{
		this(transport1.getTransportString() + "://" + host1 + ":" + port1, socketType1, shouldBind1, 
				transport2.getTransportString() + "://" + host2 + ":" + port2, socketType2, shouldBind2);
	}
	
	/**
	 * The constructor of the MessageBroker class instantiates a MessageBroker object.
	 * @param address1 The address string consisting of transport type, host, and port.
	 * @param socketType1 The type of socket created.
	 * @param shouldBind1 Whether to bind the socket to the address as opposed to connecting.
	 * @param address2 The address string consisting of transport type, host, and port.
	 * @param socketType2 The type of socket created.
	 * @param shouldBind2 Whether to bind the socket to the address as opposed to connecting.
	 */
	public MessageBroker(String address1, TYPE socketType1, boolean shouldBind1,
			String address2, TYPE socketType2, boolean shouldBind2)
	{
		receiverAddress = address1;
		receiver = new BasicSocket(receiverAddress, socketType1, shouldBind1);
		forwarderAddress = address2;
		forwarder = new BasicSocket(forwarderAddress, socketType2, shouldBind2);
		
		ZMQ.proxy(receiver.getSocket(), forwarder.getSocket(), null);
	}

	/**
	 * Getter for receiver's address.
	 * @return
	 */
	public String getReceiverAddress()
	{
		return receiverAddress;
	}
	
	/**
	 * Getter for forwarder's address
	 * @return
	 */
	public String getForwarderAddress()
	{
		return forwarderAddress;
	}
}
