package edu.gatech.adamlab.genii.io.socket.shared;

import java.util.HashSet;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.google.gson.Gson;

/**
 * A basic socket that can create either a server or client or similar of various types and transports.
 * @author mikhail.jacob
 *
 */
public class BasicSocket
{
	/**
	 * The ZMQ Context for creating and controlling sockets.
	 */
	private Context context;
	
	/**
	 * The ZMQ Socket object that implements socket I/O.
	 */
	private Socket socket;
	
	/**
	 * The address used to connect or bind the socket.
	 */
	private String address;
	
	/**
	 * Object of TYPE enumerator that conveys the type of socket.
	 */
	private TYPE socketType;
	
	/**
	 * Whether the socket binds or connects to the port.
	 */
	private boolean isBind;
	
	/**
	 * Google GSON class object converts other objects to and from JSON.
	 */
	private Gson gson;
	
	/**
	 * Set of filters that are used to subscribe to a publisher if socketType is TYPE.SUB, null otherwise.
	 */
	private HashSet<String> filters;
	
	/**
	 * String setting the delimiter between the filter String and the actual serialization of an object.
	 */
	private static final String FILTER_DELIMITER = "###>>>@<<<###";
	
	/**
	 * The constructor of the BasicSocket class instantiates a BasicSocket object.
	 * @param transport TRANSPORT enumerator used to choose transport protocol / method.
	 * @param host Local or remote host to connect or bind to.
	 * @param port The port to connect or bind to.
	 * @param socketType The type of socket created.
	 * @param isBind Whether to bind the socket to the address as opposed to connecting.
	 */
	public BasicSocket(TRANSPORT transport, String host, int port, TYPE socketType, boolean isBind)
	{
		this(transport.getTransportString() + "://" + host + ":" + port, socketType, isBind);
	}
	
	/**
	 * The constructor of the BasicSocket class instantiates a BasicSocket object.
	 * @param address The address string consisting of transport type, host, and port.
	 * @param socketType The type of socket created.
	 * @param isBind Whether to bind the socket to the address as opposed to connecting.
	 */
	public BasicSocket(String address, TYPE socketType, boolean isBind)
	{
		try
		{
			this.address = address;
			context = ZMQ.context(1);
			this.socketType = socketType;
			socket = context.socket(socketType.getSocketType());
			
			this.isBind = isBind;
			if(isBind)
			{
				socket.bind(address);
			}
			else
			{
				socket.connect(address);
			}
			
			gson = new Gson();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Filters the stream of incoming messages for those with a specific filter String.
	 * @param filter The String used to filter.
	 */
	public void subscribe(String filter)
	{
		try
		{
			if(socket == null)
			{
				throw new Exception("Socket was null.");
			}
			if(socketType != TYPE.SUB)
			{
				throw new Exception("The subscribe() method only applies to sockets of type SUBSCRIBER");
			}
			
			if(filters == null)
			{
				filters = new HashSet<String>();
			}
			if(!filters.contains(filter))
			{
				filters.add(filter);
			}

			socket.subscribe(filter.getBytes());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes a filter String from a socket.
	 * @param filter The String filter that is to be removed.
	 */
	public void unsubscribe(String filter)
	{
		try
		{
			if(socket == null)
			{
				throw new Exception("Socket was null.");
			}
			if(socketType != TYPE.SUB)
			{
				throw new Exception("The subscribe() method only applies to sockets of type SUBSCRIBER.");
			}
			if(filters == null || !filters.contains(filter))
			{
				throw new Exception("The socket is not currently subscribed to this message.");
			}
			
			socket.unsubscribe(filter.getBytes());
			
			filters.remove(filter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Overrides traditional finalize() method of Object class to dispose of socket and context.
	 */
	@Override
	protected void finalize() throws Throwable
	{
		try
		{
			if(isBind)
			{
				socket.unbind(address);
			}
			else
			{
				socket.disconnect(address);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(socket != null)
			{
				socket.close();
				socket = null;
			}
			if(context != null)
			{
				context.term();
				context = null;
			}
		}
		
		super.finalize();
	}
	
	/**
	 * Method used to explicitly unbind or disconnect a socket, close it, and 
	 * terminate the context after the socket is finished being used.
	 */
	public void stop()
	{
		try
		{
			if(isBind)
			{
				socket.unbind(address);
			}
			else
			{
				socket.disconnect(address);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(socket != null)
			{
				socket.close();
				socket = null;
			}
			if(context != null)
			{
				context.term();
				context = null;
			}
		}
	}
	
	/**
	 * Enumerator with types of transport methods or protocols available to use.
	 * @author mikhail.jacob
	 *
	 */
	public static enum TRANSPORT
	{
		INPROC, //inter-thread communication
		IPC, //Inter-process communication
		PGM, //Pragmatic General Multicast
		EPGM, //Encapsulate PGM in UDP
		TCP; // Transport Control Protocol
		
		public String getTransportString()
		{
			return toString().toLowerCase();
		}
	}
	
	/**
	 * Enumerator with types of sockets that can be created using ZMQ.
	 * @author mikhail.jacob
	 *
	 */
	public static enum TYPE
	{
		REQ, //REQUEST send to server from client
		REP, //REPLY received to client from server
		PUB, //PUBLISHER sends data to optional SUBSCRIBERs
		SUB, //SUBSCRIBER receives filtered data from a PUBLISHER
		XPUB, //EXTENDED PUBLISHER that receives a subscriber message from a publisher and forwards that to an EXTENDED SUBSCRIBER
		XSUB, //EXTENDED SUBSCRIBER that receives a message from an EXTENDED PUBLISHER and forwards that to a subscriber 
		PUSH, //PUSH to a sink from a source
		PULL, //PULL from a source to a sink
		DEALER, //DEALER 
		ROUTER, //ROUTER
		PAIR; //EXLUSIVE PAIR
		
		public int getSocketType() throws Exception
		{
			try
			{
				switch (valueOf(toString()))
				{
					case REQ: //REQUEST send to server from client
					{
						return ZMQ.REQ;
					}
					case REP: //REPLY received to client from server
					{
						return ZMQ.REP;
					}
					case PUB: //PUBLISHER sends data to optional SUBSCRIBERs
					{
						return ZMQ.PUB;
					}
					case SUB: //SUBSCRIBER receives filtered data from a PUBLISHER
					{
						return ZMQ.SUB;
					}
					case XPUB: //EXTENDED PUBLISHER that receives a subscriber message from a publisher and forwards that to an EXTENDED SUBSCRIBER
					{
						return ZMQ.XPUB;
					}
					case XSUB: //EXTENDED SUBSCRIBER that receives a message from an EXTENDED PUBLISHER and forwards that to a subscriber
					{
						return ZMQ.XSUB;
					} 
					case PUSH: //PUSH to a sink from a source
					{
						return ZMQ.PUSH;
					}
					case PULL: //PULL from a source to a sink
					{
						return ZMQ.PULL;
					}
					case DEALER: //DEALER
					{
						return ZMQ.DEALER;
					} 
					case ROUTER: //ROUTER
					{
						return ZMQ.ROUTER;
					}
					case PAIR: //EXLUSIVE PAIR
					{
						return ZMQ.PAIR;
					}
					default:
					{
						throw new Exception("Unknown socket type.");
					}
				}
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	/**
	 * Use send() to send an object over this socket after converting to GSON's JSON String representation.
	 * @param object The Object to send.
	 * @param type The class definition of that Object.
	 * @param blocking Whether the send should be blocking or not
	 */
	public <T> void send(Object object, Class<T> type, boolean blocking)
	{
		try
		{
			if(socket == null)
			{
				throw new Exception("Tried send() when socket was null.");
			}
			if(socketType == TYPE.SUB)
			{
				throw new Exception("Can't call send() method on a SUBSCRIBER socket.");
			}
			
			if(blocking)
			{
				socket.send(gson.toJson(object, type));
			}
			else
			{
				socket.send(gson.toJson(object, type), ZMQ.DONTWAIT);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Use sendFiltered() to send an object over this socket using a specific filter after converting to GSON's JSON String 
	 * representation.
	 * @param object The Object to send.
	 * @param type The class definition of that Object.
	 * @param filter The String that filters the incoming stream of data for subscribed 
	 * messages
	 * @param blocking Whether the send should be blocking or not
	 */
	public <T> void sendFiltered(Object object, Class<T> type, String filter, boolean blocking)
	{
		try
		{
			if(socket == null)
			{
				throw new Exception("Tried send() when socket was null.");
			}
			if(!(socketType == TYPE.PUB || socketType == TYPE.XPUB))
			{
				throw new Exception("Can't call sendFiltered() method on a SUBSCRIBER socket.");
			}
			
			String message = String.format("%s" + FILTER_DELIMITER + "%s", filter, gson.toJson(object, type));
			
			if(blocking)
			{
				socket.send(message);
			}
			else
			{
				socket.send(message, ZMQ.DONTWAIT);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Use receive() to receive objects over this socket after converting back from String using GSON's JSON conversion library
	 * @param type The class definition of the object being received
	 * @param blocking Whether the send should be blocking or not
	 * @return
	 */
	public <T> T receive(Class<T> type, boolean blocking)
	{
		try
		{
			if(socket == null)
			{
				throw new Exception("Tried receive() when socket was null.");
			}
			if(socketType == TYPE.PUB)
			{
				throw new Exception("Can't call receive() method on a PUBLISHER socket.");
			}
			
			String receivedString = null;
			
			if(blocking)
			{
				receivedString = socket.recvStr().trim();
			}
			else
			{
				receivedString = socket.recvStr(ZMQ.DONTWAIT).trim();
			}
			
			if(receivedString == null)
			{
				return null;
			}
			
			if(socketType == TYPE.SUB || socketType == TYPE.XSUB)
			{
				receivedString = receivedString.split(FILTER_DELIMITER)[1];
			}
			
			return gson.fromJson(receivedString, type);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			return null;
		}
	}

	/**
	 * Getter for address.
	 * @return
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * Getter for Socket Type.
	 * @return
	 */
	public TYPE getSocketType()
	{
		return socketType;
	}
	
	/**
	 * Getter for Socket
	 * @return
	 */
	public Socket getSocket()
	{
		return socket;
	}

	/**
	 * Getter for isBind
	 * @return
	 */
	public boolean isBind()
	{
		return isBind;
	}	
}