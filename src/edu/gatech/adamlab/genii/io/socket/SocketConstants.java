package edu.gatech.adamlab.genii.io.socket;

import edu.gatech.adamlab.genii.io.socket.shared.BasicSocket.TYPE;

/**
 * Class defines constants and other shared resources that are used to instantiate sockets.
 * @author mikhail.jacob
 *
 */
public class SocketConstants
{
	//SocketManager Constants
	
	//ADDRESS CONSTANTS
	
	//Default transport is assumed to be TCP
	//Default address is LOCALHOST or 127.0.0.1
	//EPHEMERAL PORT RANGE: 49152–65535
	
//	/**
//	 * The address string for the Action client socket.
//	 */
//	public static final String ACTION_CLIENT_ADDRESS = "tcp://localhost:50000";

	/**
	 * The address string for the Action server socket.
	 */
	public static final String ACTION_ADDRESS = "tcp://localhost:50001";

	/**
	 * The address string for the Knowledge Library client socket.
	 */
	public static final String KNOWLEDGE_LIBRARY_CLIENT_ADDRESS = "tcp://localhost:50010";

	/**
	 * The address string for the Knowledge Library server socket.
	 */
	public static final String KNOWLEDGE_LIBRARY_SERVER_ADDRESS = "tcp://localhost:50011";

	/**
	 * The address string for the Perception client socket.
	 */
	public static final String PERCEPTION_ADDRESS = "tcp://localhost:50020";

//	/**
//	 * The address string for the Perception server socket.
//	 */
//	public static final String PERCEPTION_SERVER_ADDRESS = "tcp://localhost:50021";

	/**
	 * The address string for the Web Data client socket.
	 */
	public static final String WEB_DATA_CLIENT_ADDRESS = "tcp://localhost:50030";

	/**
	 * The address string for the Web Data server socket.
	 */
	public static final String WEB_DATA_SERVER_ADDRESS = "tcp://localhost:50031";

	/**
	 * The address string for the Web Feedback client socket
	 */
	public static final String WEB_FEEDBACK_CLIENT_ADDRESS = "tcp://localhost:50040";

	/**
	 * The address string for the Web Feedback server socket.
	 */
	public static final String WEB_FEEDBACK_SERVER_ADDRESS = "tcp://localhost:50041";

	//FILTER CONSTANTS
	
	/**
	 * The string used to subscribe to percepts from the publisher socket.
	 */
	public static final String PERCEPTION_FILTER = "PERCEPTION";
	
	/**
	 * The string used to subscribe to acts from the publisher socket.
	 */
	public static final String ACTION_FILTER = "ACTION";
	
	//SOCKET TYPE CONSTANTS
	
	/**
	 * The socket type of the perception socket.
	 */
	public static final TYPE PERCEPTION_TYPE = TYPE.SUB;

	/**
	 * The socket type of the action socket.
	 */
	public static final TYPE ACTION_TYPE = TYPE.PUB;
	
	//IS BIND CONSTANTS

	/**
	 * Whether the perception socket should bind or connect.
	 */
	public static final boolean IS_PERCEPTION_BIND = false;

	/**
	 * Whether the action socket should bind or connect.
	 */
	public static final boolean IS_ACTION_BIND = true;
}
