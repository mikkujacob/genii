package edu.gatech.adamlab.genii.io.socket;

/**
 * An abstract interface to wrap objects containing data that has to be sent or received over a socket.
 * @author mikhail.jacob
 *
 */
public abstract interface AbstractSocketData
{
	/**
	 * Derived classes have to define how to convert the wrapped data into a String representation.
	 * @return
	 */
	public abstract String convertToString();
}
