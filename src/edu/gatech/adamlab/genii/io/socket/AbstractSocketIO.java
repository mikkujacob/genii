package edu.gatech.adamlab.genii.io.socket;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * The parent abstraction for input, output, and input-output socket classes used to communicate
 * between this application and others. Contains a couple of LinkedBlockingQueues that serve to get
 * inputs from and set outputs to the external environment. The class must be parameterized by two
 * data types representing socket data to and from the sockets. Any kind of data can be wrapped in
 * these classes.
 * @author mikhail.jacob
 *
 * @param <T> The wrapper for data that is input to the socket for sending.
 * @param <U> The wrapper for data that is output from the socket after receiving.
 */
public abstract class AbstractSocketIO<T extends AbstractSocketData, U extends AbstractSocketData> implements Runnable
{
	/**
	 * A concurrency-safe queue that can get inputs from the external environment that is sent over a socket.
	 */
	protected LinkedBlockingQueue<T> inputQueue;
	
	/**
	 * A concurrency-safe queue that can set outputs to the external environment that is received over a socket.
	 */
	protected LinkedBlockingQueue<U> outputQueue;
	
	/**
	 * A default constructor to create the two queues.
	 */
	public AbstractSocketIO()
	{
		inputQueue = new LinkedBlockingQueue<T>();
		outputQueue = new LinkedBlockingQueue<U>();
	}
	
	/**
	 * A method that needs to be called in derived classes after setting parameters for socket creation in order to create the sockets and start processing data over them. 
	 */
	protected void initialize()
	{
		createSockets();
		
		(new Thread(this)).start();
	}
	
	/**
	 * This method is abstract and needs to be implemented by derived classes. This is where BasicSockets are created and any subscription or other methods are called on them before data can be sent / received over them. 
	 */
	public abstract void createSockets();
	
	/**
	 * This is a method to add an object of type T to the input queue externally in order to send it over a socket.
	 * @param inputObject The object to be sent over the socket.
	 * @return Whether it was successful or not.
	 */
	public boolean addInputToQueue(T inputObject)
	{
		return inputQueue.offer(inputObject);
	}
	
	/**
	 * This is a method to remove an object of type T from the input queue internally in order to send it over a socket.
	 * @return The object to be sent over the socket.
	 */
	protected T removeInputFromQueue()
	{
		return inputQueue.poll();
	}
	
	/**
	 * This is a method to add an object of type U to the output queue internally after receiving it over a socket.
	 * @param outputObject The object that was received over the socket.
	 * @return Whether it was successful or not.
	 */
	protected boolean addOutputToQueue(U outputObject)
	{
		if(outputObject == null)
		{
			return false;
		}
		else
		{
			return outputQueue.offer(outputObject);
		}
	}
	
	/**
	 * This is a method to remove an object of type U from the output queue externally after receiving it over a socket.
	 * @return The object that was received over the socket.
	 */
	public U removeOutputFromQueue()
	{
		return outputQueue.poll();
	}
}