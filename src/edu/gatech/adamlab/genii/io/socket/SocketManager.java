package edu.gatech.adamlab.genii.io.socket;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.gatech.adamlab.genii.io.socket.action.ActionOutputSocket;
import edu.gatech.adamlab.genii.io.socket.action.ActionSocketInputData;
import edu.gatech.adamlab.genii.io.socket.perception.PerceptionInputSocket;
import edu.gatech.adamlab.genii.io.socket.perception.PerceptionSocketOutputData;

/**
 * The SocketManager class creates sockets for communication between GENII
 * and other applications, data sinks, and data sources. 
 * @author mikhail.jacob
 *
 */
public class SocketManager
{
	/**
	 * Is the SocketManager thread currently running. Toggle this to control the thread.
	 */
	private boolean isRunning;

	/**
	 * An input socket that receives percepts from an external transmitter.
	 */
	PerceptionInputSocket perceptionIO;
	
	/**
	 * An output socket that sends actions to an external receiver.
	 */
	ActionOutputSocket actionIO;
	
	PerceptionSocketOutputData currentPerceptionData;
	
	/**
	 * Public constructor that instantiates, initializes, and starts running the sockets.
	 */
	public SocketManager()
	{
		perceptionIO = new PerceptionInputSocket(
				SocketConstants.PERCEPTION_ADDRESS, 
				SocketConstants.PERCEPTION_TYPE, 
				SocketConstants.IS_PERCEPTION_BIND, 
				SocketConstants.PERCEPTION_FILTER
				);
		actionIO = new ActionOutputSocket(
				SocketConstants.ACTION_ADDRESS, 
				SocketConstants.ACTION_TYPE, 
				SocketConstants.IS_ACTION_BIND, 
				SocketConstants.ACTION_FILTER
				);
		
		isRunning = true;
		
		final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
		
		scheduledExecutorService.scheduleWithFixedDelay(
			new Runnable()
			{
				@Override
				public void run()
				{
					if(!isRunning)
					{
						scheduledExecutorService.shutdownNow();
					}
					runSocket();
				}
			}, 0, 50, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Internal run loop for the socket manager that transmits and receives socket data.
	 */
	public void runSocket()
	{
		//Receive percepts on the PerceptionInputSocket
		PerceptionSocketOutputData data = perceptionIO.removeOutputFromQueue();
		if(data != null)
		{
			currentPerceptionData = data;
		}
	}
	
	/**
	 * Returns the last percept received over the perceptionIO socket 
	 * if the socket is running and has received at least one percept.
	 * Otherwise return null
	 * @return - Last percept received if one was received and socket 
	 * is running, or null.
	 */
	public PerceptionSocketOutputData getPercept()
	{
		if(isRunning && currentPerceptionData != null)
		{
			PerceptionSocketOutputData data = currentPerceptionData;
			currentPerceptionData = null;
			return data;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Sets the action to be sent over the actionIO socket if the 
	 * socket is running.
	 * @param action - The action to be sent over the actionIO socket.
	 * @return - Whether action was added to the socket to be sent 
	 * successfully or not.
	 */
	public boolean setAction(ActionSocketInputData action)
	{
		if(isRunning)
		{
			return actionIO.addInputToQueue(action);
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Get whether the socket manager is running.
	 * @return - Whether socket manager is running.
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * Set whether the socket manager is running.
	 * @param isRunning - Whether to set the socket manager to run or not.
	 */
	public void setRunning(boolean isRunning)
	{
		this.isRunning = isRunning;
	}
}