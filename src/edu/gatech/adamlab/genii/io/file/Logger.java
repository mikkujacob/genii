/**
 * 
 */
package edu.gatech.adamlab.genii.io.file;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.File;

/**
 * @author mikhailjacob
 *
 */
public class Logger
{
	private final static Boolean isDebugOutputEnabled = true;
	private final static Boolean isLogEnabled = true;
	private static String logFilePath = "data/logs/log-";
	private static BufferedWriter logWriter;
	
	public static void log(String message, boolean isNewLine)
	{
		if(isLogEnabled)
		{
			//Log to file
			try
			{
				//If the log writer hasn't been initialized and opened yet.
				if(logWriter == null)
				{
					//Rename the file according to the current date and time.
					logFilePath = logFilePath + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()) + ".log";
					//Create a new file if it doesn't exist yet.
					
					System.out.println("PATH: " + logFilePath);
					
					new File(logFilePath).createNewFile();
					logWriter = new BufferedWriter(new FileWriter(logFilePath));
					//Create a shutdown hook to close the log writer on application shutdown.
					Runtime.getRuntime().addShutdownHook(new Thread()
					{
						public void run() 
						{
							try
							{
								logWriter.flush();
								logWriter.close();
							}
							catch (IOException e)
							{
								e.printStackTrace();
							}
						}
					});
				}
				
				if(logWriter != null)
				{
					if(isNewLine)
					{
						message = message + "\n";
					}
					
					//Write the log to file.
					logWriter.write(message);
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		if(isDebugOutputEnabled)
		{
			System.out.print(message);
		}
	}
	
	public static void log(String message)
	{
		log(message, true);
	}
	
	public static void log()
	{
		log("", true);
	}
}
