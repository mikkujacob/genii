package edu.gatech.adamlab.genii.io.osc;

import java.io.IOException;
import java.net.SocketException;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import edu.gatech.adamlab.genii.io.file.Logger;
import edu.gatech.adamlab.genii.io.osc.javaosc.OSCListener;
import edu.gatech.adamlab.genii.io.osc.javaosc.OSCMessage;
import edu.gatech.adamlab.genii.io.osc.javaosc.OSCPortIn;
import edu.gatech.adamlab.genii.io.osc.javaosc.OSCPortOut;
import edu.gatech.adamlab.genii.io.osc.statedefinition.KinectStatePercept;
import edu.gatech.adamlab.genii.io.osc.statedefinition.ObjectStatePercept;

public class GENIIMImEOSCBridge
{
	OSCPortIn receiver;
	OSCPortOut GENIIToMImEBridge;
	GENIIOSCListener listener;
	
	ConcurrentLinkedQueue<OSCMessage> messageQueue;
	
	public static void main(String[] args)
	{
		GENIIMImEOSCBridge test = new GENIIMImEOSCBridge();
		OSCMessage message;
		String text;
		//Basic echo server
		while(true)
		{
			if(test.hasMessage())
			{
				message = test.getNextMessage();
				text = (String) message.getArguments().get(0);
				Logger.log("Received Message: " + text);
				test.sendMessage(text);
				
				ObjectStatePercept percept = new ObjectStatePercept(text);
				Logger.log("Received object: " + percept.toString());
			}
			
			try
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public GENIIMImEOSCBridge()
	{
		try
		{
			receiver = new OSCPortIn(GENIIOSCConstants.ListenerPort);
			GENIIToMImEBridge = new OSCPortOut(GENIIOSCConstants.getRemoteIP(), GENIIOSCConstants.SendToPort);
			messageQueue = new ConcurrentLinkedQueue<>();
			listener = new GENIIOSCListener(messageQueue);
			receiver.addListener("/" + GENIIOSCConstants.MImEToGENIIAddress, listener);
			receiver.startListening();
		}
		catch (SocketException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean hasMessage()
	{
		return !messageQueue.isEmpty();
	}
	
	public OSCMessage getNextMessage()
	{
		return messageQueue.poll();
	}
	
	public void sendMessage(String message)
	{
		OSCMessage oscMessage = new OSCMessage("/" + GENIIOSCConstants.GENIIToMImEAddress);
		oscMessage.addArgument(message);
		try
		{
			GENIIToMImEBridge.send(oscMessage);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public ObjectStatePercept getObjectStatePercept(OSCMessage message)
	{
		String jsonObjectString = (String) message.getArguments().get(0);
		return new ObjectStatePercept(jsonObjectString);
	}
	
	public KinectStatePercept getKinectStatePercept(OSCMessage message)
	{
		String jsonObjectString = (String) message.getArguments().get(0);
		return new KinectStatePercept(jsonObjectString);
	}
}

class GENIIOSCListener implements OSCListener
{
	ConcurrentLinkedQueue<OSCMessage> queue;
	
	public GENIIOSCListener(ConcurrentLinkedQueue<OSCMessage> queue)
	{
		this.queue = queue;
	}

	@Override
	public void acceptMessage(Date time, OSCMessage message)
	{
//		Logger.log("Received a message: {Address: " + message.getAddress() + ", Values: [" + message.getArguments().get(0) + ", " + message.getArguments().get(1) + "]}");
		if(message.getAddress().equals("/" + GENIIOSCConstants.MImEToGENIIAddress))
		{
			queue.add(message);
		}
	}
}