package edu.gatech.adamlab.genii.io.osc;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class GENIIOSCConstants
{
	public static final String remoteIP = "127.0.0.1";
	public static final int SendToPort = 57132;
    public static final int ListenerPort= 57133;
    public static final String MImEToGENIIAddress = "MImEToGENII";
    public static final String GENIIToMImEAddress = "GENIIToMImE";
    public static final String ObjectStatePerceptCSharpName = "ObjectStateDefinition";
    public static final String KinectStatePerceptCSharpName = "KinectStateDefinition";
    public static final String StartActionPerceptCSharpName = "StartAction";
    public static final String StartActionStartPerceptCSharpName = "StartAction.Start";
    public static final String StartActionEndPerceptCSharpName = "StartAction.End";
    public static final String EndActionPerceptCSharpName = "EndAction";
    public static final String EndActionStartPerceptCSharpName = "EndAction.Start";
    public static final String EndActionEndPerceptCSharpName = "EndAction.End";
    
    public static InetAddress getRemoteIP()
    {
    	try
		{
			return InetAddress.getByName(remoteIP);
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
			return null;
		}
    }
}
