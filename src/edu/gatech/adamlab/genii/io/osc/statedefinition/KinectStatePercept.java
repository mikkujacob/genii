/**
 * 
 */
package edu.gatech.adamlab.genii.io.osc.statedefinition;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;

import edu.gatech.adamlab.genii.memory.hypergraph.Constants.JIDX;
import edu.gatech.adamlab.genii.shared.Body;
import edu.gatech.adamlab.genii.shared.PVector;
import edu.gatech.adamlab.genii.shared.Quaternion;

/**
 * @author mikhailjacob
 *
 */
public class KinectStatePercept
{
	private static Gson gson = new Gson();
	
	private ArrayList<PVector> jointPositions;
	private ArrayList<Quaternion> jointAngles;
	private int anchorJoint;
	private JIDX anchorJointIndex;
	
	public KinectStatePercept(String jsonObjectString)
	{
		this(gson.fromJson(jsonObjectString, KinectStatePercept.class));
	}
	
	public KinectStatePercept(KinectStatePercept kinectStatePercept)
	{
		this.jointPositions = kinectStatePercept.jointPositions;
		this.jointAngles = kinectStatePercept.jointAngles;
		this.anchorJoint = kinectStatePercept.anchorJoint;
		this.anchorJointIndex = JIDX.values()[kinectStatePercept.anchorJoint];
	}
	
	public Body getBody()
	{
		if(jointPositions == null || jointAngles == null || jointPositions.isEmpty() || jointAngles.isEmpty() || anchorJointIndex == null)
		{
			return null;
		}
		
		HashMap<JIDX, Quaternion> angles = new HashMap<JIDX, Quaternion>();
		for(int index = 0; index < JIDX.values().length; index++)
		{
			angles.put(JIDX.values()[index], jointAngles.get(index));
		}
		
		HashMap<JIDX, PVector> positions = new HashMap<JIDX, PVector>();
		for(int index = 0; index < JIDX.values().length; index++)
		{
			positions.put(JIDX.values()[index], jointPositions.get(index));
		}
		Body body = new Body(positions, angles, this.anchorJointIndex);
		return body;
	}

	/**
	 * @return the jointPositions
	 */
	public ArrayList<PVector> getJointPositions()
	{
		return jointPositions;
	}

	/**
	 * @param jointPositions the jointPositions to set
	 */
	public void setJointPositions(ArrayList<PVector> jointPositions)
	{
		this.jointPositions = jointPositions;
	}

	/**
	 * @return the jointAngles
	 */
	public ArrayList<Quaternion> getJointAngles()
	{
		return jointAngles;
	}

	/**
	 * @param jointAngles the jointAngles to set
	 */
	public void setJointAngles(ArrayList<Quaternion> jointAngles)
	{
		this.jointAngles = jointAngles;
	}

	/**
	 * @return the anchorJoint
	 */
	public int getAnchorJoint()
	{
		return anchorJoint;
	}

	/**
	 * @param anchorJoint the anchorJoint to set
	 */
	public void setAnchorJoint(int anchorJoint)
	{
		this.anchorJoint = anchorJoint;
	}

	/**
	 * @return the anchorJointIndex
	 */
	public JIDX getAnchorJointIndex()
	{
		return anchorJointIndex;
	}

	/**
	 * @param anchorJointIndex the anchorJointIndex to set
	 */
	public void setAnchorJointIndex(JIDX anchorJointIndex)
	{
		this.anchorJointIndex = anchorJointIndex;
	}
	
	/**
	 * Convert Kinect State Percept to String.
	 */
	public String toString()
	{
		return gson.toJson(this, KinectStatePercept.class);
	}
}
