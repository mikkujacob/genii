/**
 * 
 */
package edu.gatech.adamlab.genii.io.osc.statedefinition;

import java.util.ArrayList;
import java.util.UUID;

import com.google.gson.Gson;

import edu.gatech.adamlab.genii.shared.PVector;
import edu.gatech.adamlab.genii.shared.Quaternion;
import edu.gatech.adamlab.genii.shared.TrackedChange;

/**
 * @author mikhailjacob
 *
 */
public class ObjectStatePercept
{
	private static Gson gson = new Gson();
	
	private UUID uuid;
	private String id;
	private boolean linked;
	private ArrayList<String> links;
	private boolean animate;
	private boolean contact;
	private ArrayList<String> contacts;
	private boolean open;
	private boolean containing;
	private ArrayList<String> contains;
	private boolean contained;
	private ArrayList<String> containers;
	private PVector location;
	private Quaternion orientation;
	private ArrayList<String> above;
	private ArrayList<String> below;
	private ArrayList<String> behind;
	private ArrayList<String> ahead;
	private ArrayList<String> rightOf;
	private ArrayList<String> leftOf;
	private ArrayList<String> near;
	private ArrayList<String> far;
	private ArrayList<TrackedChange> changes;
	
	public ObjectStatePercept(String jsonObjectString)
	{
		this(gson.fromJson(jsonObjectString, ObjectStatePercept.class));
		this.uuid = UUID.fromString(this.id);
	}
	
	private ObjectStatePercept(ObjectStatePercept percept)
	{
		id = percept.id;
		this.uuid = UUID.fromString(this.id);
		linked = percept.linked;
		animate = percept.animate;
		contact = percept.contact;
		open = percept.open;
		containing = percept.containing;
		contains = new ArrayList<String>(percept.contains);
		contained = percept.contained;
		containers = new ArrayList<String>(percept.containers);
		location = percept.location;
		orientation = percept.orientation;
		above = new ArrayList<String>(percept.above);
		below = new ArrayList<String>(percept.below);
		behind = new ArrayList<String>(percept.behind);
		ahead = new ArrayList<String>(percept.ahead);
		rightOf = new ArrayList<String>(percept.rightOf);
		leftOf = new ArrayList<String>(percept.leftOf);
		near = new ArrayList<String>(percept.near);
		far = new ArrayList<String>(percept.far);
		changes = new ArrayList<TrackedChange>(percept.changes);
	}
	
	/**
	 * Convert Object State Percept to String.
	 */
	public String toString()
	{
		return gson.toJson(this, ObjectStatePercept.class);
	}

	/**
	 * @return the uuid
	 */
	public UUID getUuid()
	{
		return uuid;
	}

	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(UUID uuid)
	{
		this.uuid = uuid;
	}

	/**
	 * @return the changes
	 */
	public ArrayList<TrackedChange> getChanges()
	{
		return changes;
	}

	/**
	 * @param changes the changes to set
	 */
	public void setChanges(ArrayList<TrackedChange> changes)
	{
		this.changes = changes;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the linked
	 */
	public boolean isLinked() {
		return linked;
	}

	/**
	 * @param linked the linked to set
	 */
	public void setLinked(boolean linked) {
		this.linked = linked;
	}

	/**
	 * @return the links
	 */
	public ArrayList<String> getLinks() {
		return links;
	}

	/**
	 * @param links the links to set
	 */
	public void setLinks(ArrayList<String> links) {
		this.links = links;
	}

	/**
	 * @return the animate
	 */
	public boolean isAnimate() {
		return animate;
	}

	/**
	 * @param animate the animate to set
	 */
	public void setAnimate(boolean animate) {
		this.animate = animate;
	}

	/**
	 * @return the contact
	 */
	public boolean isContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(boolean contact) {
		this.contact = contact;
	}

	/**
	 * @return the contacts
	 */
	public ArrayList<String> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts the contacts to set
	 */
	public void setContacts(ArrayList<String> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the open
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * @param open the open to set
	 */
	public void setOpen(boolean open) {
		this.open = open;
	}

	/**
	 * @return the containing
	 */
	public boolean isContaining() {
		return containing;
	}

	/**
	 * @param containing the containing to set
	 */
	public void setContaining(boolean containing) {
		this.containing = containing;
	}

	/**
	 * @return the contains
	 */
	public ArrayList<String> getContains() {
		return contains;
	}

	/**
	 * @param contains the contains to set
	 */
	public void setContains(ArrayList<String> contains) {
		this.contains = contains;
	}

	/**
	 * @return the contained
	 */
	public boolean isContained() {
		return contained;
	}

	/**
	 * @param contained the contained to set
	 */
	public void setContained(boolean contained) {
		this.contained = contained;
	}

	/**
	 * @return the containers
	 */
	public ArrayList<String> getContainers() {
		return containers;
	}

	/**
	 * @param containers the containers to set
	 */
	public void setContainers(ArrayList<String> containers) {
		this.containers = containers;
	}

	/**
	 * @return the location
	 */
	public PVector getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(PVector location) {
		this.location = location;
	}

	/**
	 * @return the orientation
	 */
	public Quaternion getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Quaternion orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the above
	 */
	public ArrayList<String> getAbove() {
		return above;
	}

	/**
	 * @param above the above to set
	 */
	public void setAbove(ArrayList<String> above) {
		this.above = above;
	}

	/**
	 * @return the below
	 */
	public ArrayList<String> getBelow() {
		return below;
	}

	/**
	 * @param below the below to set
	 */
	public void setBelow(ArrayList<String> below) {
		this.below = below;
	}

	/**
	 * @return the behind
	 */
	public ArrayList<String> getBehind() {
		return behind;
	}

	/**
	 * @param behind the behind to set
	 */
	public void setBehind(ArrayList<String> behind) {
		this.behind = behind;
	}

	/**
	 * @return the ahead
	 */
	public ArrayList<String> getAhead() {
		return ahead;
	}

	/**
	 * @param ahead the ahead to set
	 */
	public void setAhead(ArrayList<String> ahead) {
		this.ahead = ahead;
	}

	/**
	 * @return the rightOf
	 */
	public ArrayList<String> getRightOf() {
		return rightOf;
	}

	/**
	 * @param rightOf the rightOf to set
	 */
	public void setRightOf(ArrayList<String> rightOf) {
		this.rightOf = rightOf;
	}

	/**
	 * @return the leftOf
	 */
	public ArrayList<String> getLeftOf() {
		return leftOf;
	}

	/**
	 * @param leftOf the leftOf to set
	 */
	public void setLeftOf(ArrayList<String> leftOf) {
		this.leftOf = leftOf;
	}

	/**
	 * @return the near
	 */
	public ArrayList<String> getNear() {
		return near;
	}

	/**
	 * @param near the near to set
	 */
	public void setNear(ArrayList<String> near) {
		this.near = near;
	}

	/**
	 * @return the far
	 */
	public ArrayList<String> getFar() {
		return far;
	}

	/**
	 * @param far the far to set
	 */
	public void setFar(ArrayList<String> far) {
		this.far = far;
	}
}
