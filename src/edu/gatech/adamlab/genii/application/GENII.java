/**
 * 
 */
package edu.gatech.adamlab.genii.application;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.gatech.adamlab.genii.io.file.Logger;
import edu.gatech.adamlab.genii.io.osc.GENIIMImEOSCBridge;
import edu.gatech.adamlab.genii.io.osc.GENIIOSCConstants;
import edu.gatech.adamlab.genii.io.osc.javaosc.OSCMessage;
import edu.gatech.adamlab.genii.io.socket.SocketManager;
import edu.gatech.adamlab.genii.io.socket.perception.PerceptionSocketOutputData;
import edu.gatech.adamlab.genii.memory.hypergraph.HyperGraphMemoryModel;

/**
 * @author mikhailjacob
 * The main application class for instantiating and using the GENII (Graphical Embodied Narrative Intelligence & Improvisation) system.
 */
public class GENII
{
	private static final boolean useOSC = true;
	
	/**
	 * The socket manager that control communication over sockets.
	 */
	private SocketManager socketManager;
	
	/**
	 * The OSC socket communication bridge between GENII and MImE.
	 */
	private GENIIMImEOSCBridge oscBridge;
	
	/**
	 * String that holds the last command from the console.
	 */
	static String command = "";
	
	/**
	 * The hypergraph memory model used by the system to store perceptual data as a long term memory (LTM). 
	 */
	private HyperGraphMemoryModel memoryModel;
	
	/**
	 * Boolean that indicates whether an action from MImE has started
	 */
	private boolean hasActionStarted;
	
	/**
	 * Boolean that indicates whether an action from MImE has started and the initial state of all tracked objects is being updated in GENII.
	 */
	private boolean isStartActionObjectStateUpdate;
	
	/**
	 * Boolean that indicates whether an action from MImE has end and the final state of all tracked objects is being updated in GENII.
	 */
	private boolean isEndActionObjectStateUpdate;
	
	/**
	 * public constructor for the GENII application class.
	 */
	public GENII()
	{
		memoryModel = new HyperGraphMemoryModel();
		Logger.log("\nHyperGraph Memory Model:\t" + memoryModel.toString());
		
		if(useOSC)
		{
			oscBridge = new GENIIMImEOSCBridge();
			Logger.log("OSC Bridge initialized and started.");
		}
		else
		{
			socketManager = new SocketManager();
			Logger.log("Socket Manager initialized and started.");
		}
		
		hasActionStarted = false;
		isStartActionObjectStateUpdate = false;
		isEndActionObjectStateUpdate = false;
	}
	
	/**
	 * Main method to start the GENII application execution.
	 * @param args
	 */
	public static void main(String[] args)
	{
		Logger.log("GENII booting up...");
		final GENII application = new GENII();
		//Create an ExecutorService to repeatedly and regularly attempt the updation of Viewpoints AI
		final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
		
		scheduledExecutorService.scheduleWithFixedDelay(
				new Runnable()
				{
					final Scanner commandScanner = new Scanner(System.in);
			        
					@Override
					public void run()
			        {
						command = commandScanner.nextLine();
			        }
			    }, 0, 500, TimeUnit.MILLISECONDS); 
		
		scheduledExecutorService.scheduleWithFixedDelay(
				new Runnable()
				{
					@Override
					public void run()
					{
						if(command.equalsIgnoreCase("exit") || command.equalsIgnoreCase("quit") 
		        			|| command.equalsIgnoreCase("done") || command.equalsIgnoreCase("finish") 
		        			|| command.equalsIgnoreCase("end") || command.equalsIgnoreCase("return"))
						{
							Logger.log("GENII shutting down...");
							application.shutdown();
							scheduledExecutorService.shutdownNow();
							System.exit(0);
						}
						application.update();
					}
				}, 0, 1, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Update method to update the GENII agent with new percepts.
	 */
	public void update()
	{
		if(useOSC)
		{
			if(oscBridge.hasMessage())
			{
				OSCMessage message = oscBridge.getNextMessage();
				if(message != null)
				{
					Logger.log("Current percept: " + message.getArguments().toString());
					if(message.getArguments().get(1).equals(GENIIOSCConstants.StartActionPerceptCSharpName))
					{
						if(!hasActionStarted)
						{
							hasActionStarted = true;
							Logger.log("Action started");
							memoryModel.interpretStartActionPercept(false);
							//Is the start action percept starting the object state update.
							if(message.getArguments().get(0).equals(GENIIOSCConstants.StartActionStartPerceptCSharpName))
							{
								isStartActionObjectStateUpdate = true;
							}
						}
						else if(isStartActionObjectStateUpdate && message.getArguments().get(0).equals(GENIIOSCConstants.StartActionEndPerceptCSharpName))
						{
							memoryModel.interpretStartActionPercept(true);
							//Is the start action percept ending the object state update.
							isStartActionObjectStateUpdate = false;
						}
						else
						{
							//TODO: Handle two unexpectedly consecutive StartActionPercepts intelligently
						}
					}
					else if(message.getArguments().get(1).equals(GENIIOSCConstants.EndActionPerceptCSharpName))
					{
						if(hasActionStarted)
						{
							hasActionStarted = false;
							Logger.log("Action Ended");
							memoryModel.interpretEndActionPercept(false);
							//Is the end action percept starting the object state update.
							if(message.getArguments().get(0).equals(GENIIOSCConstants.EndActionStartPerceptCSharpName))
							{
								isEndActionObjectStateUpdate = true;
							}
						}
						else if(isEndActionObjectStateUpdate && message.getArguments().get(0).equals(GENIIOSCConstants.EndActionEndPerceptCSharpName))
						{
							memoryModel.interpretEndActionPercept(true);
							//Is the end action percept ending the object state update.
							isEndActionObjectStateUpdate = false;
						}
						else
						{
							//TODO: Handle two unexpectedly consecutive EndActionPercepts intelligently
						}
					}
					else if((hasActionStarted || isEndActionObjectStateUpdate) && message.getArguments().get(1).equals(GENIIOSCConstants.ObjectStatePerceptCSharpName))
					{
						memoryModel.interpretObjectStatePercept(oscBridge.getObjectStatePercept(message));
					}
					else if(hasActionStarted && message.getArguments().get(1).equals(GENIIOSCConstants.KinectStatePerceptCSharpName))
					{
						memoryModel.interpretKinectStatePercept(oscBridge.getKinectStatePercept(message));
					}
					else
					{
						Logger.log("Unhandled percept: " + message.getArguments().toString());
					}
				}
			}
		}
		else
		{
			PerceptionSocketOutputData percept = socketManager.getPercept();
			if(percept != null)
			{
				Logger.log("Current percept: " + percept.convertToString());
				//Process percept
				memoryModel.interpretPerceptionInputSocketPercept(percept);
			}
		}		
	}
	
	/**
	 * Method to free allocated resources and shutdown the GENII application. 
	 */
	public void shutdown()
	{
		memoryModel.closeGraph();
		socketManager.setRunning(false);
	}
}
